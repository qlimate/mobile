angular.module('qlimate.controllers.pages').controller('MenuCtrl', ($rootScope, state, $scope, transport, $state, storage, $u) => {
	$scope.list = [{
		href: 'map',
		name: 'Карта'
	}, {
		href: 'shop',
		name: 'Магазин'
	}, /*{
		href: 'tournament',
		name: 'Турнир'
	}, */{
		href: 'feedback',
		name: 'Помощь'
	}, {
		href: 'profile',
		name: 'Профиль'
	}];
	$scope.$on('$ionicView.afterEnter', (viewInfo, {stateName}) => {
		if (stateName && /^menu\./.test(stateName))
			$scope.currentPage = stateName.replace(/^menu\./, '');
	});
	$scope.logout = () => {
		$u.showBackdrop();
		let destroy = () => {
			storage.token = null;
			storage.phone = null;
			storage.team = null;
			storage.nickname = null;
			state.authed = false;
			transport.stopSocket();
			$state.go(`preload`);
			$rootScope.team = 'neutral';
		};
		transport.send(transport.type.POST, 'logout').then(destroy, destroy).then(() => $u.hideBackdrop());
	};
	$scope.version = '%version%';
});
