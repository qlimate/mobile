angular.module('qlimate.controllers.pages').controller('ChooseCtrl', ($scope, state, storage, $state, $u, toastr) => {
	$scope.$on('$ionicView.afterEnter', () => {
		toastr.info('PAGE ||| Choose');
		if (storage.team || storage.intendedTeam)
			$scope.setTeam(storage.team || storage.intendedTeam);
	});
	$scope.setTeam = color => {
		storage.intendedTeam = color;
		$state.go('preload');
	};
});