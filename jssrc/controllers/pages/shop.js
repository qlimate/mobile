angular.module('qlimate.controllers.pages').controller('ShopCtrl', ($scope, $timeout, transport, $u, icons, state) => {
	$scope.iconMap = icons.items;
	$scope.items = [];
	$scope.selectedIndex = 0;
	$scope.barrel = {};

	$scope.$on('$ionicView.afterEnter', () => {
		$scope.placeholder = 'Загружаем магазин..';
		transport.send(transport.type.GET, 'store').then(({data}) => {
			$scope.$apply(() => $scope.items = data);
			min = elementHeight*($scope.items.length - 1);
		}, ({code, description}) => {
			if (code === 402)
				$scope.placeholder = 'Нет сети';
			else
				$u.showAlert(description);
		});
	});

	let elementHeight = document.body.scrollWidth*0.2; // HARDCODED: 20vw
	let min = elementHeight, max = 0, top = 0, topRaw = 0;
	Object.defineProperty($scope.barrel, 'margin-top', { enumerable: true, get: () => `calc(20vw - ${Math.abs(topRaw)}px)`});
	$scope.doDrag = event => {
		$scope.barrel['transition'] = '';
		topRaw = Math.min(Math.max(max, top - event.gesture.deltaY), min);
	};
	$scope.doRelease = () => $scope.move(Math.min($scope.items.length - 1, Math.max(0, Math.round(topRaw/elementHeight))));
	$scope.move = id => {
		top = topRaw = id*elementHeight;
		$scope.selectedIndex = id;
		$scope.barrel['transition'] = 'all 0.2s ease-in';
	};

	$scope.buy = () => {
		$u.showBackdrop();
		transport.send(transport.type.POST, 'inventory', {
			index: $scope.items[$scope.selectedIndex].index
		}).then(() => {
			$u.showPopup({title: 'Успех', message: `${$scope.items[$scope.selectedIndex].name} куплен! Ищи его в инвентаре.`});
			$scope.items[$scope.selectedIndex].count--;
			if($scope.items[$scope.selectedIndex].count === 0)
				$scope.items.splice($scope.selectedIndex, 1)
		}, ({code, description}) => {
			$u.showAlert(code === 4316 ? 'К сожалению, такие предметы кончились' : 'К сожалению, не получилось купить предмет.');
		}).then(() => $u.hideBackdrop());
	}
});