angular.module('qlimate.controllers.pages').controller('PreloadCtrl', ($scope, storage, $state, state, transport, $u, socket, toastr) => {
	const REQUIRED = [
		{
			ready: () => storage.intendedTeam !== undefined,
			resolve: () => $state.go('choose-side')
		},
		{
			ready: () => state.authed,
			resolve: () => {
				if (storage.token && storage.phone)
					transport
						.send(transport.type.POST, 'login', {
							phone: storage.phone,
							token: storage.token
						})
						.then(() => {
							state.authed = true;
							state.forceUpdate()
								.then(() => state.current.name ? null : transport.send(transport.type.PUT, 'profile', { name: storage.nickname }))
								.then(() => storage.addPhoneNamePair(storage.phone, state.current.name)).catch(e => {console.log('PUT on profile failed: ' + JSON.stringify(e))});
						}, () => storage.token = null)
						.then(() => $scope.$emit('$ionicView.afterEnter'));
				else
					$state.go('auth');
			}
		}
	];
	$scope.$on('$ionicView.afterEnter', () => {
		toastr.info('PAGE ||| Preload');
		var left = REQUIRED.filter(condition => !condition.ready());
		if (left.length)
			return left[0].resolve();
		transport.socketAuth();
		$state.go('menu.map');
	});
});