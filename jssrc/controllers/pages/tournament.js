angular.module('qlimate.controllers.pages').controller('TournamentCtrl', ($scope, transport, $u, $state) => {
	$scope.$on('$ionicView.afterEnter', () => {
		transport.send(transport.type.GET, 'tournaments').then(({data}) => {
			$scope.tournaments = data.items;
		});
	});

	$scope.$on('$ionicView.beforeEnter', () => $u.showPopup({title: 'Внимание!', message: 'Раздел в разработке. Приносим свои извинения.'}).then(() => $state.go('menu.map')));
});