angular.module('qlimate.controllers.pages').controller('MapCtrl', ($scope, $ionicPopover, terminals, state, $cordovaBarcodeScanner, transport, $u, $timeout, toastr, location) => {
	$scope.$on('$ionicView.afterEnter', () => {
		toastr.info('PAGE ||| Map');
		state.forceUpdate();
	});
	document.getElementById('map').onmousedown = e => e.preventDefault() & false;

	mapboxgl.accessToken = 'pk.eyJ1IjoibHVpeG8iLCJhIjoiZjlhMTVlOGY2N2MyNjUxM2FhOGYyZjgyZGYwODdjNzIifQ.5TM2WTaF4_Y08t6K68gnNw';
	$scope.map = new mapboxgl.Map({
		container: 'map',
		style: state.current.team === -1 ? 'mapbox://styles/luixo/cih7etdg0002bbkko2mhpmlh0' : 'mapbox://styles/luixo/cii7j48nk00grbpm0kw6pqfg4',
		center: [37.5972, 55.6372],
		zoom: 9
	});
	$scope.map.addControl(new mapboxgl.Navigation({position: 'bottom-left'}));
	$scope.map.dragRotate && $scope.map.dragRotate.disable();
	$scope.map.touchZoomRotate && $scope.map.touchZoomRotate.disableRotation();

	state.onUpdate(({team}) => {
		if (team !== $scope.team)
			$scope.map.setStyle(state.current.team === -1 ? 'mapbox://styles/luixo/cih7etdg0002bbkko2mhpmlh0' : 'mapbox://styles/luixo/cii7j48nk00grbpm0kw6pqfg4');
		$scope.team = team;
	});

	const updateTerms = position => {
		Promise.all([terminals.list(position), new Promise(resolve => $scope.map.style.loaded() ? resolve() : $scope.map.on('style.load', resolve))]).then(([{plrs, terms}]) => {
			var data = {
				type: 'FeatureCollection',
				features: Object.values(terms).map(({id, description, position, temperature = -0.5 + Math.random()}) =>
					({
						type: 'Feature',
						geometry: {
							type: 'Point',
							coordinates: [position.lng, position.lat]
						},
						properties: {
							title: 'Terminal',
							'marker-symbol': `terminal-${temperature === 0 ? 'neutral' : temperature > 0 ? 'hot' : 'cold'}`,
							id,
							description,
							temperature
						}
					})
				)
			};
			$scope.map.getSource('markers') ? $scope.map.getSource('markers').setData(data) : $scope.map.addSource('markers', {
				type: 'geojson',
				data,
				cluster: true,
				clusterRadius: 40
			});

			if (!$scope.map.getLayer('markers'))
				$scope.map.addLayer({
					id: 'markers',
					interactive: true,
					type: 'symbol',
					source: 'markers',
					layout: {
						'icon-image': '{marker-symbol}',
						'icon-allow-overlap': true
					}
				});

			if (!$scope.map.getLayer('markers-cluster'))
				$scope.map.addLayer({
					id: 'markers-cluster',
					interactive: true,
					type: 'symbol',
					source: 'markers',
					layout: { 'icon-image': 'terminals' },
					filter: ['>', 'point_count', 1]
				});
		}, error => {});
	};
	location.update().then(updateTerms);
	$scope.map.on('moveend', ({target}) => updateTerms(target.getCenter()));

	let zoomHelper = null, mapPopup = null;
	$scope.map.on('zoomstart', e => zoomHelper = e.target.getZoom());
	$scope.map.on('zoomend', e => mapPopup && zoomHelper < e.target.getZoom() ? mapPopup.remove() : null);

	const onClick = e => {
		$scope.map.featuresAt(e.point, {
			radius: 25,
			layer: 'markers'
		}, (err, features) => {
			features = features.reduce((mem, feature) => feature.properties.cluster ? mem.concat(feature.properties.points) : mem.concat([feature]), []);
			if (features.length) {
				mapPopup = new mapboxgl.Popup()
					.setLngLat(e.lngLat)
					.setHTML(`
						<div class="head">Терминалы</div>
						<dl>
							${features.reduce((memo, {properties: term}, i) => memo + (i === 5 ? '<div class="ion-search"></div>' : i < 5 ? `
							<dt class="${term.temperature === 0 ? 'neutral' : term.temperature > 0 ? 'hot' : 'cold'}">${term.description.replace(/^Россия,? ?/, '').replace(/^Москва,? ?/, '')}</dt>
							<dd>${Math.round(term.temperature)}°</dd>
						` : ''), '')}</dl>
					`)
					.addTo($scope.map);
				(document.querySelectorAll('.mapboxgl-popup .ion-search')[0] || {}).onclick = () => { $scope.map.flyTo({center: [e.lngLat.lng, e.lngLat.lat], zoom: $scope.map.getZoom() + 1}) }
			}
		})
	};
	$scope.map.on('click', e => onClick(e));

	$u.showBackdrop();
	// HACK: убрали копирайт. Юридически это неправильно.
	$scope.map._controlCorners['bottom-right'].innerHTML = '';
	$scope.findMe = () => location.update().then(({lng, lat, accuracy}) => $scope.map.flyTo({center: [lng, lat], zoom: accuracy === 0 ? 10 : 16 - (accuracy/500)}));
	$scope.findMe();
	Promise.loop((resolve,reject) => $timeout(() => transport.socketStatus ? reject() : resolve(transport.socketAuth()), 300)).catch(() => $u.hideBackdrop());
});