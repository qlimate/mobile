angular.module('qlimate.controllers.pages').controller('FeedbackCtrl', ($scope, transport, $u, $state) => {
	$scope.send = () => {
		if (!$scope.feedback)
			return $u.showAlert('Письмо пустое!');
		console.log($scope);
		$u.showBackdrop();
		transport.send(transport.type.PUT, 'feedback', {
			message: $scope.feedback || ''
		}).then(() => {
			$u.hideBackdrop();
			$scope.feedback = '';
			$u.showPopup({title: 'Супер', message: 'Письмо ушло! Жди ответа.'}).then(() => $state.go('menu.map'));
		}, () => {
			$u.hideBackdrop();
			$u.showAlert('Мы не смогли отправить письмо. Попробуй еще раз.');
		});
	};
});