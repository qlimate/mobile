angular.module('qlimate.controllers.pages').controller('AuthCtrl', ($scope, storage, state, $state, transport, $u, socket, toastr) => {
	$scope.$on('$ionicView.afterEnter', () => {
		toastr.info('PAGE ||| Login');
		if (storage.phone)
			$scope.phone = storage.phone.toString();
		$scope.showPin = false;
	});
	$scope.storage = storage;
	$scope.doRequestPin = phone => {
		$u.showBackdrop();
		if($scope.nickname)
			storage.nickname = $scope.nickname;
		storage.phone = phone;
		transport.send(transport.type.POST, 'register', {phone}).then(({data: {code} = {}}) => {
			$scope.$apply(() => {
				$scope.showPin = true;
				$scope.pin = Number(code) || '';
			});
		}).catch(({description, code}) => {
			$u.showAlert(description);
		}).then(() => $u.hideBackdrop());
	};
	$scope.doConfirm = (phone, code) => {
		$u.showBackdrop();
		transport.send(transport.type.POST, 'confirm', {
			phone,
			code,
			team: storage.intendedTeam === 1 ? 'orange' : storage.intendedTeam === -1 ? 'blue' : 'neutral'
		}).then(({data: {token} = {}}) => {
			storage.nickname = storage.nickname || $scope.nickname || $scope.generate;
			storage.token = token;
			$state.go('preload');
		}).catch(({description, code}) => {
			$scope.$apply(() => {
				$scope.showPin = false;
				$scope.pin = '';
			});
			if (code === 4312) {
				$u.showAlert('Вы не выбрали команду!');
				storage.team = null;
				return $state.go('preload');
			}
			$u.showAlert(description);
			// TODO: Ошибка "ник занят"
		}).then(() => $u.hideBackdrop())
	};
	$scope.changeTeam = color => storage.intendedTeam = color;

	$scope.generate = (() => {
		var adj = ['Зимний', 'Красный', 'Черный', 'Огненный'];
		var noun = ['волк', 'лев', 'олень', 'кракен', 'дракон', 'орел', 'змей'];
		return adj[Math.floor(Math.random()*adj.length)] + ' ' + noun[Math.floor(Math.random()*noun.length)];
	})();
}).directive('formatPhone', () => ({
	require: '?ngModel',
	link: (scope, elem, attrs, ctrl) => {
		if (!ctrl) return;
		ctrl.$formatters.unshift(modelValue => (modelValue || '')
			.replace(/^(7)(\d{3})(.+)/, '+$1($2)$3') // Add braces around city code
			.replace(/^(\+7.{5})(\d{3})(.+)/g, '$1$2-$3')       // Add first dash
			.replace(/^(\+7.{9})(\d{2})(.+)/g, '$1$2-$3')
		);
		ctrl.$parsers.unshift(viewValue => {
			var phone = viewValue
				.replace(/([^\d+]|\+)/g, '')    // Remove garbage symbols w/ not first plus signes
				.replace(/^([0-69])$/, '7$1')   // Add 7 at the beginning
				.replace(/^8(\d)/, '7$1')        // Modify '8917..' to '7917..'
				.substring(0, 11);
			elem.val(ctrl.$formatters[0](phone));
			return phone;
		});

		ctrl.$validators.phone = model => !!(model && model.length === 11);
	}
}));