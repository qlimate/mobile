angular.module('qlimate.controllers.pages').controller('ProfileCtrl', ($scope, transport, state, icons, $u) => {
	state.onUpdate(({name, phone, team}) => {
		$scope.team = team;
		$scope.name = name;
		$scope.chars = {'Телефон': phone};
	});

	$scope.iconMap = icons.items;
	$scope.items = [];
	$scope.selectedIndex = 0;
	$scope.barrel = {};

	$scope.$on('$ionicView.afterEnter', () => {
		$scope.placeholder = 'Загружаем предметы..';
		transport.send(transport.type.GET, 'inventory').then(({data}) => {
			$scope.items = data;
			min = elementHeight*($scope.items.length + 1);
			top = topRaw = min;
		}, ({code, description}) => {
			if (code === 402)
				$scope.placeholder = 'Нет сети';
			else
				$u.showAlert(description);
		});
	});

	let elementHeight = document.body.scrollWidth*0.15; // HARDCODED: 15vw
	let min = 0, max, top, topRaw, topMargin = 10;
	top = topRaw = max = elementHeight*2;
	Object.defineProperty($scope.barrel, 'margin-top', { enumerable: true, get: () => `calc(${100 - topMargin}vh - ${Math.abs(topRaw)}px)`});
	$scope.doDrag = event => {
		$scope.barrel['transition'] = '';
		topRaw = Math.min(Math.max(max, top - event.gesture.deltaY), min);
	};
	$scope.doRelease = () => $scope.move(Math.min($scope.items.length - 1, Math.max(0, Math.round(topRaw/elementHeight) - 2)));
	$scope.move = id => {
		top = topRaw = (id + 2)*elementHeight;
		$scope.selectedIndex = id;
		$scope.barrel['transition'] = 'all 0.2s ease-in';
	};

	$scope.use = () => {
		return $u.showPopup({title: 'Внимание', message: 'К сожалению, этот функционал пока не реализован.'});
		$u.showBackdrop();
		transport.send(transport.type.PUT, 'inventory', {
			index: $scope.items[$scope.selectedIndex].index
		}).then(() => {
			$u.showPopup({title: 'Успех', message: 'Предмет использован!'});
		}, () => {
			$u.showAlert('К сожалению, не получилось использовать предмет.');
		}).then(() => $u.hideBackdrop());
	}
});