angular.module('qlimate.controllers.elements').directive('energyBar', () => {
	var buttonWidth = 80;
	return {
		restrict: 'E',
		templateUrl: 'templates/energy-bar.html',
		replace: true,
		controller: ($scope, $element, state) => {
			$scope.max = 1;
			$scope.current = 0;
			var boltWidth = 6;
			var energyBoltWidth = $element[0].ownerDocument.body.children[0].clientHeight * 0.01 * boltWidth;
			var lightningsCount = Math.floor(($element[0].ownerDocument.body.clientWidth - energyBoltWidth*(2 /* menu button size */ + 4 /* energy available size */))/energyBoltWidth) - 1;
			$scope.total = new Array(lightningsCount);
			state.onUpdate(state =>
				$scope.$apply(() => {
					$scope.max = state.max > 0 ? state.max : $scope.max;
					$scope.current = state.current > 0 ? state.current : $scope.current;
					$scope.pivot = ($scope.current / $scope.max) * $scope.total.length;
				})
			);
		}
	}
});