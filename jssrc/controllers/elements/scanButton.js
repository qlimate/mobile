angular.module('qlimate.controllers.elements').directive('scanButton', ($cordovaBarcodeScanner, $ionicModal, $u, transport, toastr, location) => {
	return {
		restrict: 'E',
		templateUrl: 'templates/scan-button.html',
		replace: true,
		controller: ($scope, $element, state) => {
			$scope.max = 0;
			$scope.current = 0;
			state.onUpdate(state => {
				$scope.max = state.max;
				$scope.current = state.current;
			});

			let min = 800 - 185,
				max = 800 - 185 - Math.round(($element[0].ownerDocument.body.scrollHeight - 185)/2),
				bottom, bottomRaw;
			const reset = () => {
				bottom = bottomRaw = -min;
				$scope.energyPercent = 0.1;
			};
			reset();
			$scope.draggedStyle = { get bottom() { return `${bottomRaw}px` } };
			$scope.doDrag = event => {
				bottomRaw = Math.max(Math.min(-max, bottom - event.gesture.deltaY), -min);
				$scope.energyPercent = Math.max((min + bottomRaw)/(min - max), 0.1);
			};
			$scope.doRelease = event => bottom = bottomRaw;

			$ionicModal.fromTemplateUrl('templates/scan.html', {
				scope: $scope,
				backdropClickToClose: false
			}).then(modal => $scope.modal = modal);

			$scope.scan = $event => {
				(window.cordova ? $cordovaBarcodeScanner.scan() : Promise.resolve({
					format: 'mock',
					cancelled: false,
					text: 'qlimate.com/15&1111'
					//text: 'asdkl;'
				}))
					.then(({format, cancelled, text}) => {
						if (cancelled)
							return Promise.reject({code: 1, description: 'Сканирование отменено пользователем'});
						toastr.info(`Barcode cancelled status ${cancelled}`, `Barcode format ${format}`);
						var decoded = text.replace('qlimate.com/s?', '').split('&');
						return Promise.resolve(decoded.length === 2 ? decoded : ['','']);
					}, () => Promise.reject({code: 1, description: 'Сканирование отменено пользователем'}))
					.then(([id = '', secret = '']) => {
						if (!id || id.length > 12 || !secret || secret.length !== 4)
							return Promise.reject({code: 500, description: 'Сканированный QR-код некорректен.'});
						if ($scope.energyPercent > 1)
							return Promise.reject({code: 500, description: 'Некорректное количество энергии.'});
						return transport.send(transport.type.POST, 'scan', {id, secret, energy: Math.max(Math.round($scope.energyPercent*$scope.current), 1), lat: location.lat, lng: location.lng})
					})
					.catch(({code, description} = {}) => {
						toastr.warning(`Barcode was not scanned: ${description}`);
						if (code === 4325)
							description = 'Неправильный QR-код! Попробуй еще раз.';
						return Promise.reject({
							code: code || 400,
							description: description || 'Кажется, отсутствует сеть'
						})
					})
					.then(({data}) => {
						toastr.info(`QR data response: ${JSON.stringify(data)}`);
						//data = {"delta":{"player":{"energy":-1,"capacity":0.01},"terminal":{"temperature":-0.5}}};
						$scope.scanResults = [];
						if (data.delta) {
							if (data.delta.player) {
								if (data.delta.player.energy)
									$scope.scanResults.push({
										title: 'Энергия игрока изменена',
										description: `Ты потратил ${-data.delta.player.energy || 0} энергии!`
									});

								if (data.delta.player.capacity)
									$scope.scanResults.push({
										title: 'Максимальная энергия игрока изменена',
										description: `Теперь у тебя может быть больше на ${data.delta.player.capacity || 0} энергии!`
									});
							}
							if (data.delta.terminal) {
								if (data.delta.terminal.temperature)
									$scope.scanResults.push({
										title: 'Температура терминала изменена',
										description: `Температура терминала ${data.delta.terminal.temperature < 0 ? 'уменьшилась' : 'увеличилась'} на ${data.delta.terminal.temperature || 0}!`
									});
							}
						}
						$scope.modal.show($event);
						reset();
					}).catch(({code, description}) => {
						code === 1 ? null : $u.showAlert(description);
					});
			};
		}
	}
});