angular.module('qlimate').config(($stateProvider, $urlRouterProvider, $httpProvider) => {
	//$locationProvider.html5Mode(true);
	$httpProvider.defaults.withCredentials = true;
	$stateProvider
		.state('menu', {
			url: '/menu',
			abstract: true,
			templateUrl: 'pages/menu.html',
			controller: 'MenuCtrl'
		})
		.state('menu.map', {
			url: '/map',
			resolve: {
				root: $ionicSideMenuDelegate => $ionicSideMenuDelegate
			},
			views: {
				'menuContent': {
					templateUrl: 'pages/map.html',
					controller: 'MapCtrl'
				}
			},
			onEnter: root => setTimeout(() => root.canDragContent(false)),
			onExit: root => root.canDragContent(true),
		})
		.state('menu.shop', {
			url: '/shop',
			views: {
				'menuContent': {
					templateUrl: 'pages/shop.html',
					controller: 'ShopCtrl'
				}
			}
		})
		.state('menu.tournament', {
			url: '/tournament',
			views: {
				'menuContent': {
					templateUrl: 'pages/tournament.html',
					controller: 'TournamentCtrl'
				}
			}
		})
		.state('menu.feedback', {
			url: '/feedback',
			views: {
				'menuContent': {
					templateUrl: 'pages/feedback.html',
					controller: 'FeedbackCtrl'
				}
			}
		})
		.state('menu.profile', {
			url: '/profile',
			views: {
				'menuContent': {
					templateUrl: 'pages/profile.html',
					controller: 'ProfileCtrl'
				}
			}
		})
		.state('auth', {
			url: '/auth',
			templateUrl: 'pages/auth.html',
			controller: 'AuthCtrl'
		})
		.state('choose-side', {
			url: '/choose-side',
			templateUrl: 'pages/choose.html',
			controller: 'ChooseCtrl'
		})
		.state('preload', {
			url: '/preload',
			templateUrl: 'pages/preload.html',
			controller: 'PreloadCtrl'
		});

	$urlRouterProvider.otherwise('preload');
});