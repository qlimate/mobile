angular.module('qlimate.utils').service('location', ($cordovaGeolocation, toastr) => {
	var data = {
		lat: 55.753303 - 0.05,
		lng: 37.622487,
		accuracy: 0
	};
	var watch;
	const createWatch = () => {
		watch = $cordovaGeolocation.watchPosition({
			timeout: 3000,
			enableHighAccuracy: true
		}).then(null, err => {/* err */}, position => {
			data.lat = position.coords.latitude;
			data.lng = position.coords.longitude;
		});
	};
	return {
		stopWatch() {
			if (watch)
				$cordovaGeolocation.clearWatch(watch).then(() => watch = null, () => {/* err */});
		},
		startWatch() {
			if (!watch)
				createWatch();
		},
		update() {
			return $cordovaGeolocation.getCurrentPosition({
				timeout: 10000,
				enableHighAccuracy: false
			}).then(position => {
				toastr.warning(`Got position: ${position.coords.latitude}, ${position.coords.longitude} with accuracy ${position.coords.accuracy}`);
				data.lat = position.coords.latitude;
				data.lng = position.coords.longitude;
				data.accuracy = position.coords.accuracy;
			}).catch(error => toastr.error(`Error while getting position: ${JSON.stringify(error)}`))
				.then(() => Promise.resolve(data));
		},
		get position() { return { lat: data.lat, lng: data.lng, accuracy: data.accuracy }; },
		get lat() { return data.lat },
		get lng() { return data.lng },
		get accuracy() { return data.accuracy }
	}
});