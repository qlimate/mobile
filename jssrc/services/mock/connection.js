angular.module('qlimate.mock').service('socket', ($rootScope, $timeout, mockData, toastr) => {
	var events = {};
	return {
		on: (type, callback) => (events[type] || (events[type] = new Set())).add(callback),
		off: (type, callback) => events[type] && events[type].delete(callback),
		emit(type, data) {
			toastr.info(`MOCK >>>`, `${type}: ${JSON.stringify(data)}`);
			if (mockData[type])
				mockData[type](events, data);
			return this;
		},
		forceAuth() { this.emit('auth'); },
		isActive: true
	};
}).service('http', ($rootScope, $timeout, mockData, toastr) => {
	var response = (type, data) => {
		toastr.info(`MOCK >>>`, `${type}: ${JSON.stringify(data)}`);
		return new Promise((resolve, reject) => {
			toastr.info(`MOCK <<<`, `${type}: ${JSON.stringify(mockData[type] ? mockData[type](data) : 'No such mock')}`);
			$timeout(() => mockData[type] ? resolve({
				type,
				status: 'success',
				value: mockData[type](data),
				description: 'Successfully mocked!'
			}) : reject({
				type,
				status: 'failure',
				description: 'No mock data'
			}), 300);
		})
	};
	return {
		post: response,
		put: response,
		get: response
	}
});