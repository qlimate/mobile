angular.module('qlimate.mock').service('mockData', ($timeout, toastr) => ({
	// Socket mocks
	'auth': (events, data) => {
		toastr.info(`MOCK <<<`, `auth`);
		(events['auth'] || []).forEach(cb => cb());
		setInterval(() => {
			toastr.info(`MOCK <<<`, `update`);
			(events['update'] || []).forEach(cb => {
				cb({
					state: {
						energy: 100 + Math.random() * 20,
						capacity: 200 + Math.random() * 10,
						bonus: 20,
						penalty: 10
					}
				});
			})
		}, 5000);
		setInterval(() => {
			toastr.info(`MOCK <<<`, `notification`);
			(events['notification'] || []).forEach(cb => {
				cb({
					from: 'system',
					title: 'Текущее время: ' + Date.now(),
					body: 'Спасибо что были с нами!'
				});
			})
		}, 20000)
	},
	'map': () => {
		var terminals = [];
		var d = 0.16;
		var c = {
			lat: 55.7511,
			lng: 37.6204
		};
		for (var i = 0; i < 100; i++) {
			var r = Math.random()*d;
			terminals.push({
				position: {
					x: c.lat + (Math.random() < 0.5 ? -1 : 1)*r,
					y: c.lng + (Math.random() < 0.5 ? -1 : 1)*Math.random()*Math.sqrt(d*d - r*r)
				},
				temperature: Math.round(Math.random()*800 - 400),
				id: Math.round(Math.random()*10000)
			})
		}
		return {terminals};
	},
	'scan': () => ({
		results: [{
			title: 'Энергия',
			description: 'Энергия терминала 123 увеличена на 321',
			icon: 2,
			isNew: true
		}, {
			title: 'Достижение',
			description: 'Получено достижение "Пересмешник"',
			icon: 1,
			isNew: false
		}]
	}),
	'shop': () => [
		{
			name: 'Суперштука',
			description: 'Суперштукает предметы.',
			cost: 200
		}, {
			name: 'Суперштук 2',
			description: 'Суперштукает предметы.',
			cost: 100
		}, {
			name: 'Суперштука 3',
			description: 'Суперштукает предметы.',
			cost: 300
		}, {
			name: 'Суперштука 4',
			description: 'Суперштукает предметы.',
			cost: 400
		}
	],
	// HTTP mocks
	'login': ({phone, token}) => ({
		profile: `nwww`,
		info: `zzz`
	}),
	'register': ({phone}) => ({ code: '12345' }),
	'confirm': ({pin}) => ({ token: 'hello' }),
	'profile': () => ({
		name: 'bot13',
		phone: '7917530974378123123',
		team: 'orange'
	}),
	'info': () => ({
		game: {
			description: 'Описание игры',
			legend: 'Легенда игры'
		},
		teams: {
			orange: {
				population: 100,
				description: 'Команда огня',
				index: 1.4
			},
			blue: {
				population: 200,
				description: 'Команда льда',
				iindex: 1.2
			}
		}
	})
}));