angular.module('qlimate.utils').service('state', (transport, $timeout, toastr) => {
	var subscribers = [data => toastr.info(`State`, `${JSON.stringify(data)}`)];
	var state = {
		team: 0,
		max: 1,
		current: 0,
		bonus: 0,
		penalty: 0,
		name: '',
		phone: '',
	};
	var authed = false;
	Promise.loop(resolve =>
		transport.wait('update').then(({state: {energy, capacity, bonus, penalty}}) => {
			state = Object.assign(state, {
				current: Number(energy) >= 0 ? Number(energy) : state.current,
				max: Number(capacity) >= 0 ? Number(capacity) : state.max,
				penalty: Number(penalty) >= 0 ? Number(penalty) : state.penalty,
				bonus: Number(bonus) >= 0 ? Number(bonus) : state.bonus
			});
			subscribers.forEach(cb => cb(state));
			resolve();
		}, reason => {
			toastr.error(`State`, `Reject: ${reason}`);
			resolve();
		})
	);
	return {
		current: state,
		authed,
		onUpdate(callback) {
			subscribers.push(callback);
			this.onceUpdate(callback);
		},
		onceUpdate(callback) {
			$timeout(() => callback(state));
		},
		forceUpdate() {
			return transport.send(transport.type.GET, 'profile').then(({data: {team, name, phone}}) => {
				var teamIndex = ['blue', 'grey', 'orange'].indexOf(team);
				state.team = ~teamIndex ? teamIndex - 1 : state.team;
				state.name = name;
				state.phone = phone;
				subscribers.forEach(cb => cb(state));
			});
		}
	}
});