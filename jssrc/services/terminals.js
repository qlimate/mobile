angular.module('qlimate.utils').service('terminals', (transport, toastr, location) => {
	var terms = {};
	var plrs = {};
	const getTerms = ({lat = 0, lng = 0} = {}) =>
			transport.send(transport.type.GET, 'map', {
				lat,
				lng,
				players: false,
				terminals: true
			})
			.then(({data: {players = [], terminals = []}}) => {
				terminals.forEach(term => terms[term.id] ? null : terms[term.id] = term);
				players.forEach(player => plrs[player.id] ? null : plrs[player.id] = player);
				return ({plrs, terms});
			});
	return {
		list(position) { return getTerms(position); }
	}
});