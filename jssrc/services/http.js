angular.module('qlimate.utils').service('http', ($http, toastr) => {
	var url = `http://qlimate.com:12345/v1`;
	return {
		post: (type, data) => {
			toastr.info(`POST >>>`, `${type}: ${JSON.stringify(data)}`);
			return $http.post(`${url}/${type}`, data)
				.then(({data}) => {
					toastr.info(`POST <<<`, `${type}: ${JSON.stringify(data)}`);
					return Promise.resolve(data)
				}, (r) => Promise.reject(r.data))
		},
		put: (type, data) => {
			toastr.info(`PUT >>>`, `${type}: ${JSON.stringify(data)}`);
			return $http.put(`${url}/${type}`, data)
				.then(({data}) => {
					toastr.info(`PUT <<<`, `${type}: ${JSON.stringify(data)}`);
					return Promise.resolve(data)
				}, ({data}) => Promise.reject(data))
		},
		get: (type, data = {}) => {
			toastr.info(`GET >>>`, `${type}: ${JSON.stringify(data)}`);
			return $http.get(`${url}/${type}${Object.keys(data).length > 0 ? '?' : '/'}${Object.keys(data).map(key => `${key}=${data[key]}`).join('&')}`)
				.then(({data}) => {
					toastr.info(`GET <<<`, `${type}: ${JSON.stringify(data)}`);
					return Promise.resolve(data)
				}, ({data}) => Promise.reject(data))
		}
	}
});