angular.module('qlimate.utils').service('transport', (socket, $timeout, http) => {
	var networkTimeout = 5000;
	var errors = {
		// 1xx - Informational
		// 2xx - Success
		// 3xx - ???
		// 4xx - Technical level error
		401: ['Unknown server error', 'Неизвестная ошибка сервера'],
		402: ['Connection timeout', 'Превышено время ожидания ответа сервера'],
		403: ['Not authorized', 'Пользователь не авторизован'],
		404: ['Required resource is not available', 'Запрашиваемый ресурс отсутствует'],
		405: ['Javascript error', 'Ошибка в коде приложения.'],
		406: ['Unknown javascript error', 'Непонятная шибка в коде приложения.'],
		// 5xx - Data completeness error
		// 6xx - Game logic error
	};
	/*
	 * Transport module
	 * Used to send or wait for messages for unified socket-like module with 'on', 'off' and 'emit' methods.
	 */
	return {
		/*
		 * Used to send socket message of specified type
		 * @param {type} type of message
		 * @param {data} message
		 * @returns {Promise} resolve with value or reject with fail message
		 */
		send(transportType, type, data) {
			return Promise.race([$timeout(() => Promise.reject({code: 402, description: 'Нет сети'}), networkTimeout), new Promise((resolve, reject) => {
				switch (transportType) {
					case this.type.WEBSOCKET:
						socket.emit(type, data).on(type, resolve);
						break;
					case this.type.POST:
					case this.type.GET:
					case this.type.PUT:
						http[transportType](type, data)
							.then(({status, value, description, id}) => {
								if (!status || status === 'failure')
									return Promise.reject({description: description || 'Неизвестная ошибка', code: id});
								resolve(value)
							})
							.catch(reason => reason && typeof reason === 'object'
								? reject({code: reason.code || -1, description: reason.description || 'Неизвестная ошибка'})
								: reject({code: 402, description: (reason && reason.toString()) || 'Нет сети'}));
						break;
				}
			})]).then(data => {
				if (transportType === this.type.WEBSOCKET)
					socket.off(type);
				return Promise.resolve({code: 100, data: data || {}})
			}).catch(reason => {
				if (typeof reason === 'string') {
					if (/Uncaught.*Error:.*/.test(reason))
						reason = {code: 405};
					else
						reason = {code: 401};
				}
				else if (reason.hasOwnProperty('code')) {
					if (reason.code === 5)
						reason.code = 500;
				}
				else
					reason = {code: 406};
				return Promise.reject({
					code: reason.code,
					description: reason.description || errors[reason.code][1] || 'Неописанная ошибка.'
				});
			})
		},
		/*
		 * Used to wait for socket response
		 * @param {type} type of response
		 * @returns {Promise} resolve with value or reject with fail message
		 */
		wait(type) {
			return new Promise(resolve =>
				socket.on(type, function response(data) {
					resolve(data);
					socket.off(type, response);
				})
			);
		},
		socketAuth() { return socket.forceAuth(); },
		stopSocket() { return socket.forceStop(); },
		get socketStatus() { return socket.isActive; },
		type: {
			GET: 'get',
			POST: 'post',
			PUT: 'put',
			WEBSOCKET: 'ws'
		}
	}
});