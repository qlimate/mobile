angular.module('qlimate.utils').factory('socket', ($rootScope, storage, toastr, $u) => {
	var socket;
	var isOpen, isAuthed, reAuthId;
	var ip = `qlimate.com:54321`;
	var shown = false, stopped = false;
	var createWS = () => {
		isOpen = isAuthed = false;
		socket = new WebSocket(`ws://${ip}`);
		toastr.info(`Creating socket on ${ip}!`);
		socket.onopen = () => {
			if (isAuthed)
				return;
			shown = false;
			isOpen = true;
			clearTimeout(reAuthId);
			if (storage.token && storage.phone) {
				toastr.info(`SOCK >>>`, `auth: ${storage.token}, ${storage.phone}`);
				socket.send(JSON.stringify({type: 'auth', value: {
					token: storage.token,
					phone: storage.phone
				}}));
			}
			else
				reAuthId = setTimeout(socket.onopen, 1000);
		};
		socket.onmessage = ({data}) => {
			var {type, value, status, message} = JSON.parse(data);
			toastr.info(`SOCK <<<`, `${type}: ${JSON.stringify(value)}`);
			if (type === 'auth') {
				delayed.forEach(({type, data}) => socket.send(JSON.stringify({type, value: data || null})));
				delayed = [];
				return isAuthed = true;
			}
			subscribers.filter(sub => sub.type === type).forEach(sub => $rootScope.$apply(() => sub.callback(value)));
		};
		socket.onerror = e => {
			toastr.warning(`Error on socket!`, e);
			if (!shown)
				fetch.get('http://google.com/').then(
					() => $u.showAlert('Кажется, наш сервер упал!</br>Но мы уже работаем над этим.'),
					() => $u.showAlert('Кажется, у тебя не работает интернет!</br>А без него играть не получится :(')
				);
			shown = true;
		};
		socket.onclose = o => {
			toastr.warning(`Socket closed!`, o);
			if (!stopped)
				createWS();
		}
	};
	var subscribers = [];
	var delayed = [];
	return {
		on: (type, callback) => (callback && typeof callback === 'function') ? subscribers.push({type, callback}) : false,
		off: (type, callback) => subscribers = subscribers.filter(sub => !((type ? sub.type === type : true) && (callback ? sub.callback === callback : true))),
		emit(type, data) {
			if (isAuthed) {
				toastr.info(`SOCK >>>`, `${type}: ${JSON.stringify(data)}`);
				socket.send(JSON.stringify({type, value: data || null}));
			}
			else
				delayed.push({type, data});
			return this;
		},
		forceAuth: () => {
			stopped = false;
			if (!socket)
				createWS();
		},
		forceStop: () => {
			stopped = true;
			try { socket.close(); }
			catch(e) {}
		},
		get isActive() { return isOpen; }
	}
});