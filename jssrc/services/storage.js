angular.module('qlimate.utils').service('storage', ($window) => {
	var setter = (key, value) => {
		$window.localStorage.setItem(key,typeof value === 'string' ? value : JSON.stringify(value));
		if (value === null || value === undefined)
			$window.localStorage.removeItem(key);
	};
	// If not set, null or '' returns undefined
	var getter = key => {
		var res = $window.localStorage.getItem(key);
		try { res = JSON.parse(res); }
		catch(e) {}
		return res === null || res === undefined ? undefined : res;
	};
	return {
		get phone() { return getter('phone'); },
		set phone(v) { return setter('phone', v); },
		get token() { return getter('token'); },
		set token(v) { return setter('token', v); },
		get nickname() { return getter('nickname'); },
		set nickname(v) { return setter('nickname', v); },
		get team() { return getter('team'); },
		set team(v) { return setter('intendedteam', v), setter('team', v); },
		get intendedTeam() { return getter('intendedteam'); },
		set intendedTeam(v) { return setter('intendedteam', v); },
		addPhoneNamePair(phone, name) {
			let current = getter('phonenames');
			if (current.indexOf(phone) !== -1)
				setter('phonenames', `${getter('phonenames')};${phone}:${name}`);
			return getter('phonenames');
		},
		gotNameByPhone(phone) { return (getter('phonenames') || '').split(';').filter(pair => pair.split(':')[0] === phone).length > 0 }
	}
});