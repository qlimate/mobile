angular.module('qlimate.utils').service('icons', () => {
	return {
		map: {
			0: '04',
			2: '08',
			3: '31', // ?
			4: '10',
			5: '14',
			6: '18',
			7: '27' // 34
		},
		items: {
			superconductor: 'capacitor',
			battery: 'battery-full-curved',
			default: 'gear-fat'
		}
	}
});