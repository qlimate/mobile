angular.module('qlimate.utils').service('$u', ($ionicBackdrop, $ionicPopup, $timeout) => {
	var notCorrectNumber = obj => isNaN(Number(obj)) || Number(obj) === Number.POSITIVE_INFINITY || Number(obj) === Number.NEGATIVE_INFINITY;
	var backdrop = false, splash = false;
	var popups = [];
	var alert = {
		on: false,
		instance: null,
		message: '',
		title: 'Ошибка!'
	};
	return {
		correctNumber: (...rest) => rest.filter(notCorrectNumber).length === 0,
		showSplash() {
			if (splash)
				return;
			splash = true;
			document.getElementById('qlimate-splash').style.visibility = 'visible';
		},
		hideSplash() {
			if (!splash)
				return;
			splash = false;
			document.getElementById('qlimate-splash').style.visibility = 'hidden';
		},
		showBackdrop() {
			if (backdrop)
				return;
			backdrop = true;
			$ionicBackdrop.retain();
		},
		hideBackdrop() {
			if (!backdrop)
				return;
			backdrop = false;
			$ionicBackdrop.release();
		},
		showPopup: ({title, message}, buttons = [{text: 'Ок'}], destroyPolicy = 'no') => {
			// destroy mode: all, last, no
			// button: text, class, onClick
			if (destroyPolicy === 'all') {
				popups.forEach(popup => popup.close());
				popups = [];
			}
			else if (destroyPolicy === 'last')
				popups.pop().close();
			$timeout(() => {
				popups.push($ionicPopup.show({
					title,
					cssClass: 'qlimate-popup',
					template: typeof message === 'string' ? message : JSON.stringify(message),
					buttons
				}));
			});
			return $timeout(() => new Promise((res, rej) => popups[popups.length - 1].then(res)), 20);
		},
		showAlert(message) {
			return this.showPopup({title: 'Ошибка', message}, undefined, 'all');
		}
	}
});