angular.module('qlimate', ['ionic', 'qlimate.controllers', 'qlimate.controllers.pages']).run(($ionicPlatform, $ionicConfig, $rootScope, storage, state) => {
	state.onUpdate(({team}) => {
		$rootScope.team = team;
		storage.team = team || storage.team;
	});
	$ionicPlatform.ready(() => {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
		if (window.plugins && window.plugins.orientationLock && window.plugins.orientationLock.lock)
			window.plugins.orientationLock.lock();
	});
	$ionicPlatform.registerBackButtonAction(() => {}, 149);
	$ionicConfig.views.transition("none");
});

angular.module('qlimate.utils', ['toastr', 'ngAnimate']);
angular.module('qlimate.mock', ['qlimate.utils']);
angular.module('qlimate.services', ['qlimate.utils'/*, 'qlimate.mock'*/]);
angular.module('qlimate.controllers', ['qlimate.services', 'ngCordova']);
angular.module('qlimate.controllers.elements', ['qlimate.services', 'ngCordova']);
angular.module('qlimate.controllers.pages', ['qlimate.services', 'qlimate.utils', 'qlimate.controllers.elements']);

window.logLevel = 0;
window.onerror = (message, file = '', line = '', col = '', obj = {}) => {
	window.toastr ? window.toastr.error(`${message} at ${file}:${line}:${col} + ${obj.stack}`) : null;
}
Promise.loop = fn => function loop() { return new Promise(fn).then(loop, reason => Promise.reject(reason)) }();
//Promise.timeout = (fn, timeout) => Promise.loop(resolve => angular.element(document.body).injector().get('$timeout')(() => resolve(fn()), timeout));
var fetch = () => {};
fetch.get = (filename, {method, headers, content} = {}) =>
	new Promise((resolve, reject) => {
		var xhr = new XMLHttpRequest();
		if (headers)
			headers.forEach(header => xhr.setRequestHeader(header.name, header.text));
		xhr.open(method || 'GET', filename, true);
		xhr.onreadystatechange = () => {
			if (xhr.readyState === 4) {
				if (xhr.status === 200 || xhr.status === 0)
					resolve(xhr.responseText);
				xhr.onreadystatechange = null;
				reject(`Error loading ${filename} with status ${xhr.status}`);
			}
		};
		xhr.send(content || false);
	})
;