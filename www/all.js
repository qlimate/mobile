'use strict';

angular.module('qlimate', ['ionic', 'qlimate.controllers', 'qlimate.controllers.pages']).run(function ($ionicPlatform, $ionicConfig, $rootScope, storage, state) {
	state.onUpdate(function (_ref) {
		var team = _ref.team;

		$rootScope.team = team;
		storage.team = team || storage.team;
	});
	$ionicPlatform.ready(function () {
		// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
		if (window.plugins && window.plugins.orientationLock && window.plugins.orientationLock.lock) window.plugins.orientationLock.lock();
	});
	$ionicPlatform.registerBackButtonAction(function () {}, 149);
	$ionicConfig.views.transition("none");
});

angular.module('qlimate.utils', ['toastr', 'ngAnimate']);
angular.module('qlimate.mock', ['qlimate.utils']);
angular.module('qlimate.services', ['qlimate.utils' /*, 'qlimate.mock'*/]);
angular.module('qlimate.controllers', ['qlimate.services', 'ngCordova']);
angular.module('qlimate.controllers.elements', ['qlimate.services', 'ngCordova']);
angular.module('qlimate.controllers.pages', ['qlimate.services', 'qlimate.utils', 'qlimate.controllers.elements']);

window.logLevel = 0;
window.onerror = function (message) {
	var file = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];
	var line = arguments.length <= 2 || arguments[2] === undefined ? '' : arguments[2];
	var col = arguments.length <= 3 || arguments[3] === undefined ? '' : arguments[3];
	var obj = arguments.length <= 4 || arguments[4] === undefined ? {} : arguments[4];

	window.toastr ? window.toastr.error(message + ' at ' + file + ':' + line + ':' + col + ' + ' + obj.stack) : null;
};
Promise.loop = function (fn) {
	return (function loop() {
		return new Promise(fn).then(loop, function (reason) {
			return Promise.reject(reason);
		});
	})();
};
//Promise.timeout = (fn, timeout) => Promise.loop(resolve => angular.element(document.body).injector().get('$timeout')(() => resolve(fn()), timeout));
var fetch = function fetch() {};
fetch.get = function (filename) {
	var _ref2 = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

	var method = _ref2.method;
	var headers = _ref2.headers;
	var content = _ref2.content;
	return new Promise(function (resolve, reject) {
		var xhr = new XMLHttpRequest();
		if (headers) headers.forEach(function (header) {
			return xhr.setRequestHeader(header.name, header.text);
		});
		xhr.open(method || 'GET', filename, true);
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				if (xhr.status === 200 || xhr.status === 0) resolve(xhr.responseText);
				xhr.onreadystatechange = null;
				reject('Error loading ' + filename + ' with status ' + xhr.status);
			}
		};
		xhr.send(content || false);
	});
};
'use strict';

angular.module('qlimate').config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
	//$locationProvider.html5Mode(true);
	$httpProvider.defaults.withCredentials = true;
	$stateProvider.state('menu', {
		url: '/menu',
		abstract: true,
		templateUrl: 'pages/menu.html',
		controller: 'MenuCtrl'
	}).state('menu.map', {
		url: '/map',
		resolve: {
			root: function root($ionicSideMenuDelegate) {
				return $ionicSideMenuDelegate;
			}
		},
		views: {
			'menuContent': {
				templateUrl: 'pages/map.html',
				controller: 'MapCtrl'
			}
		},
		onEnter: function onEnter(root) {
			return setTimeout(function () {
				return root.canDragContent(false);
			});
		},
		onExit: function onExit(root) {
			return root.canDragContent(true);
		}
	}).state('menu.shop', {
		url: '/shop',
		views: {
			'menuContent': {
				templateUrl: 'pages/shop.html',
				controller: 'ShopCtrl'
			}
		}
	}).state('menu.tournament', {
		url: '/tournament',
		views: {
			'menuContent': {
				templateUrl: 'pages/tournament.html',
				controller: 'TournamentCtrl'
			}
		}
	}).state('menu.feedback', {
		url: '/feedback',
		views: {
			'menuContent': {
				templateUrl: 'pages/feedback.html',
				controller: 'FeedbackCtrl'
			}
		}
	}).state('menu.profile', {
		url: '/profile',
		views: {
			'menuContent': {
				templateUrl: 'pages/profile.html',
				controller: 'ProfileCtrl'
			}
		}
	}).state('auth', {
		url: '/auth',
		templateUrl: 'pages/auth.html',
		controller: 'AuthCtrl'
	}).state('choose-side', {
		url: '/choose-side',
		templateUrl: 'pages/choose.html',
		controller: 'ChooseCtrl'
	}).state('preload', {
		url: '/preload',
		templateUrl: 'pages/preload.html',
		controller: 'PreloadCtrl'
	});

	$urlRouterProvider.otherwise('preload');
});
'use strict';

angular.module('qlimate.controllers.pages').controller('MenuCtrl', function ($rootScope, state, $scope, transport, $state, storage, $u) {
	$scope.list = [{
		href: 'map',
		name: 'Карта'
	}, {
		href: 'shop',
		name: 'Магазин'
	}, /*{
    href: 'tournament',
    name: 'Турнир'
    }, */{
		href: 'feedback',
		name: 'Помощь'
	}, {
		href: 'profile',
		name: 'Профиль'
	}];
	$scope.$on('$ionicView.afterEnter', function (viewInfo, _ref) {
		var stateName = _ref.stateName;

		if (stateName && /^menu\./.test(stateName)) $scope.currentPage = stateName.replace(/^menu\./, '');
	});
	$scope.logout = function () {
		$u.showBackdrop();
		var destroy = function destroy() {
			storage.token = null;
			storage.phone = null;
			storage.team = null;
			storage.nickname = null;
			state.authed = false;
			transport.stopSocket();
			$state.go('preload');
			$rootScope.team = 'neutral';
		};
		transport.send(transport.type.POST, 'logout').then(destroy, destroy).then(function () {
			return $u.hideBackdrop();
		});
	};
	$scope.version = '%version%';
});
'use strict';

angular.module('qlimate.utils').service('device', function ($cordovaDevice) {
	var cfg = {
		/* why hello */
	};
	document.addEventListener('deviceready', function () {
		var a = $cordovaDevice.getDevice();
		console.log('Device: ' + JSON.stringify(a));
	});
});
'use strict';

angular.module('qlimate.utils').service('http', function ($http, toastr) {
	var url = 'http://qlimate.com:12345/v1';
	return {
		post: function post(type, data) {
			toastr.info('POST >>>', type + ': ' + JSON.stringify(data));
			return $http.post(url + '/' + type, data).then(function (_ref) {
				var data = _ref.data;

				toastr.info('POST <<<', type + ': ' + JSON.stringify(data));
				return Promise.resolve(data);
			}, function (r) {
				return Promise.reject(r.data);
			});
		},
		put: function put(type, data) {
			toastr.info('PUT >>>', type + ': ' + JSON.stringify(data));
			return $http.put(url + '/' + type, data).then(function (_ref2) {
				var data = _ref2.data;

				toastr.info('PUT <<<', type + ': ' + JSON.stringify(data));
				return Promise.resolve(data);
			}, function (_ref3) {
				var data = _ref3.data;
				return Promise.reject(data);
			});
		},
		get: function get(type) {
			var data = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

			toastr.info('GET >>>', type + ': ' + JSON.stringify(data));
			return $http.get(url + '/' + type + (Object.keys(data).length > 0 ? '?' : '/') + Object.keys(data).map(function (key) {
				return key + '=' + data[key];
			}).join('&')).then(function (_ref4) {
				var data = _ref4.data;

				toastr.info('GET <<<', type + ': ' + JSON.stringify(data));
				return Promise.resolve(data);
			}, function (_ref5) {
				var data = _ref5.data;
				return Promise.reject(data);
			});
		}
	};
});
'use strict';

angular.module('qlimate.utils').service('icons', function () {
	return {
		map: {
			0: '04',
			2: '08',
			3: '31', // ?
			4: '10',
			5: '14',
			6: '18',
			7: '27' // 34
		},
		items: {
			superconductor: 'capacitor',
			battery: 'battery-full-curved',
			'default': 'gear-fat'
		}
	};
});
'use strict';

angular.module('qlimate.utils').service('location', function ($cordovaGeolocation, toastr) {
	var data = {
		lat: 55.753303 - 0.05,
		lng: 37.622487,
		accuracy: 0
	};
	var watch;
	var createWatch = function createWatch() {
		watch = $cordovaGeolocation.watchPosition({
			timeout: 3000,
			enableHighAccuracy: true
		}).then(null, function (err) {/* err */}, function (position) {
			data.lat = position.coords.latitude;
			data.lng = position.coords.longitude;
		});
	};
	return Object.defineProperties({
		stopWatch: function stopWatch() {
			if (watch) $cordovaGeolocation.clearWatch(watch).then(function () {
				return watch = null;
			}, function () {/* err */});
		},
		startWatch: function startWatch() {
			if (!watch) createWatch();
		},
		update: function update() {
			return $cordovaGeolocation.getCurrentPosition({
				timeout: 10000,
				enableHighAccuracy: false
			}).then(function (position) {
				toastr.warning('Got position: ' + position.coords.latitude + ', ' + position.coords.longitude + ' with accuracy ' + position.coords.accuracy);
				data.lat = position.coords.latitude;
				data.lng = position.coords.longitude;
				data.accuracy = position.coords.accuracy;
			})['catch'](function (error) {
				return toastr.error('Error while getting position: ' + JSON.stringify(error));
			}).then(function () {
				return Promise.resolve(data);
			});
		}
	}, {
		position: {
			get: function get() {
				return { lat: data.lat, lng: data.lng, accuracy: data.accuracy };
			},
			configurable: true,
			enumerable: true
		},
		lat: {
			get: function get() {
				return data.lat;
			},
			configurable: true,
			enumerable: true
		},
		lng: {
			get: function get() {
				return data.lng;
			},
			configurable: true,
			enumerable: true
		},
		accuracy: {
			get: function get() {
				return data.accuracy;
			},
			configurable: true,
			enumerable: true
		}
	});
});
'use strict';

angular.module('qlimate.utils').factory('socket', function ($rootScope, storage, toastr, $u) {
	var socket;
	var isOpen, isAuthed, reAuthId;
	var ip = 'qlimate.com:54321';
	var shown = false,
	    stopped = false;
	var createWS = function createWS() {
		isOpen = isAuthed = false;
		socket = new WebSocket('ws://' + ip);
		toastr.info('Creating socket on ' + ip + '!');
		socket.onopen = function () {
			if (isAuthed) return;
			shown = false;
			isOpen = true;
			clearTimeout(reAuthId);
			if (storage.token && storage.phone) {
				toastr.info('SOCK >>>', 'auth: ' + storage.token + ', ' + storage.phone);
				socket.send(JSON.stringify({ type: 'auth', value: {
						token: storage.token,
						phone: storage.phone
					} }));
			} else reAuthId = setTimeout(socket.onopen, 1000);
		};
		socket.onmessage = function (_ref) {
			var data = _ref.data;

			var _JSON$parse = JSON.parse(data);

			var type = _JSON$parse.type;
			var value = _JSON$parse.value;
			var status = _JSON$parse.status;
			var message = _JSON$parse.message;

			toastr.info('SOCK <<<', type + ': ' + JSON.stringify(value));
			if (type === 'auth') {
				delayed.forEach(function (_ref2) {
					var type = _ref2.type;
					var data = _ref2.data;
					return socket.send(JSON.stringify({ type: type, value: data || null }));
				});
				delayed = [];
				return isAuthed = true;
			}
			subscribers.filter(function (sub) {
				return sub.type === type;
			}).forEach(function (sub) {
				return $rootScope.$apply(function () {
					return sub.callback(value);
				});
			});
		};
		socket.onerror = function (e) {
			toastr.warning('Error on socket!', e);
			if (!shown) fetch.get('http://google.com/').then(function () {
				return $u.showAlert('Кажется, наш сервер упал!</br>Но мы уже работаем над этим.');
			}, function () {
				return $u.showAlert('Кажется, у тебя не работает интернет!</br>А без него играть не получится :(');
			});
			shown = true;
		};
		socket.onclose = function (o) {
			toastr.warning('Socket closed!', o);
			if (!stopped) createWS();
		};
	};
	var subscribers = [];
	var delayed = [];
	return Object.defineProperties({
		on: function on(type, callback) {
			return callback && typeof callback === 'function' ? subscribers.push({ type: type, callback: callback }) : false;
		},
		off: function off(type, callback) {
			return subscribers = subscribers.filter(function (sub) {
				return !((type ? sub.type === type : true) && (callback ? sub.callback === callback : true));
			});
		},
		emit: function emit(type, data) {
			if (isAuthed) {
				toastr.info('SOCK >>>', type + ': ' + JSON.stringify(data));
				socket.send(JSON.stringify({ type: type, value: data || null }));
			} else delayed.push({ type: type, data: data });
			return this;
		},
		forceAuth: function forceAuth() {
			stopped = false;
			if (!socket) createWS();
		},
		forceStop: function forceStop() {
			stopped = true;
			try {
				socket.close();
			} catch (e) {}
		}
	}, {
		isActive: {
			get: function get() {
				return isOpen;
			},
			configurable: true,
			enumerable: true
		}
	});
});
'use strict';

angular.module('qlimate.utils').service('state', function (transport, $timeout, toastr) {
	var subscribers = [function (data) {
		return toastr.info('State', '' + JSON.stringify(data));
	}];
	var state = {
		team: 0,
		max: 1,
		current: 0,
		bonus: 0,
		penalty: 0,
		name: '',
		phone: ''
	};
	var authed = false;
	Promise.loop(function (resolve) {
		return transport.wait('update').then(function (_ref) {
			var _ref$state = _ref.state;
			var energy = _ref$state.energy;
			var capacity = _ref$state.capacity;
			var bonus = _ref$state.bonus;
			var penalty = _ref$state.penalty;

			state = Object.assign(state, {
				current: Number(energy) >= 0 ? Number(energy) : state.current,
				max: Number(capacity) >= 0 ? Number(capacity) : state.max,
				penalty: Number(penalty) >= 0 ? Number(penalty) : state.penalty,
				bonus: Number(bonus) >= 0 ? Number(bonus) : state.bonus
			});
			subscribers.forEach(function (cb) {
				return cb(state);
			});
			resolve();
		}, function (reason) {
			toastr.error('State', 'Reject: ' + reason);
			resolve();
		});
	});
	return {
		current: state,
		authed: authed,
		onUpdate: function onUpdate(callback) {
			subscribers.push(callback);
			this.onceUpdate(callback);
		},
		onceUpdate: function onceUpdate(callback) {
			$timeout(function () {
				return callback(state);
			});
		},
		forceUpdate: function forceUpdate() {
			return transport.send(transport.type.GET, 'profile').then(function (_ref2) {
				var _ref2$data = _ref2.data;
				var team = _ref2$data.team;
				var name = _ref2$data.name;
				var phone = _ref2$data.phone;

				var teamIndex = ['blue', 'grey', 'orange'].indexOf(team);
				state.team = ~teamIndex ? teamIndex - 1 : state.team;
				state.name = name;
				state.phone = phone;
				subscribers.forEach(function (cb) {
					return cb(state);
				});
			});
		}
	};
});
'use strict';

angular.module('qlimate.utils').service('storage', function ($window) {
	var setter = function setter(key, value) {
		$window.localStorage.setItem(key, typeof value === 'string' ? value : JSON.stringify(value));
		if (value === null || value === undefined) $window.localStorage.removeItem(key);
	};
	// If not set, null or '' returns undefined
	var getter = function getter(key) {
		var res = $window.localStorage.getItem(key);
		try {
			res = JSON.parse(res);
		} catch (e) {}
		return res === null || res === undefined ? undefined : res;
	};
	return Object.defineProperties({
		addPhoneNamePair: function addPhoneNamePair(phone, name) {
			var current = getter('phonenames');
			if (current.indexOf(phone) !== -1) setter('phonenames', getter('phonenames') + ';' + phone + ':' + name);
			return getter('phonenames');
		},
		gotNameByPhone: function gotNameByPhone(phone) {
			return (getter('phonenames') || '').split(';').filter(function (pair) {
				return pair.split(':')[0] === phone;
			}).length > 0;
		}
	}, {
		phone: {
			get: function get() {
				return getter('phone');
			},
			set: function set(v) {
				return setter('phone', v);
			},
			configurable: true,
			enumerable: true
		},
		token: {
			get: function get() {
				return getter('token');
			},
			set: function set(v) {
				return setter('token', v);
			},
			configurable: true,
			enumerable: true
		},
		nickname: {
			get: function get() {
				return getter('nickname');
			},
			set: function set(v) {
				return setter('nickname', v);
			},
			configurable: true,
			enumerable: true
		},
		team: {
			get: function get() {
				return getter('team');
			},
			set: function set(v) {
				return (setter('intendedteam', v), setter('team', v));
			},
			configurable: true,
			enumerable: true
		},
		intendedTeam: {
			get: function get() {
				return getter('intendedteam');
			},
			set: function set(v) {
				return setter('intendedteam', v);
			},
			configurable: true,
			enumerable: true
		}
	});
});
'use strict';

angular.module('qlimate.utils').service('terminals', function (transport, toastr, location) {
	var terms = {};
	var plrs = {};
	var getTerms = function getTerms() {
		var _ref = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

		var _ref$lat = _ref.lat;
		var lat = _ref$lat === undefined ? 0 : _ref$lat;
		var _ref$lng = _ref.lng;
		var lng = _ref$lng === undefined ? 0 : _ref$lng;
		return transport.send(transport.type.GET, 'map', {
			lat: lat,
			lng: lng,
			players: false,
			terminals: true
		}).then(function (_ref2) {
			var _ref2$data = _ref2.data;
			var _ref2$data$players = _ref2$data.players;
			var players = _ref2$data$players === undefined ? [] : _ref2$data$players;
			var _ref2$data$terminals = _ref2$data.terminals;
			var terminals = _ref2$data$terminals === undefined ? [] : _ref2$data$terminals;

			terminals.forEach(function (term) {
				return terms[term.id] ? null : terms[term.id] = term;
			});
			players.forEach(function (player) {
				return plrs[player.id] ? null : plrs[player.id] = player;
			});
			return { plrs: plrs, terms: terms };
		});
	};
	return {
		list: function list(position) {
			return getTerms(position);
		}
	};
});
'use strict';

angular.module('qlimate.utils').service('transport', function (socket, $timeout, http) {
	var networkTimeout = 5000;
	var errors = {
		// 1xx - Informational
		// 2xx - Success
		// 3xx - ???
		// 4xx - Technical level error
		401: ['Unknown server error', 'Неизвестная ошибка сервера'],
		402: ['Connection timeout', 'Превышено время ожидания ответа сервера'],
		403: ['Not authorized', 'Пользователь не авторизован'],
		404: ['Required resource is not available', 'Запрашиваемый ресурс отсутствует'],
		405: ['Javascript error', 'Ошибка в коде приложения.'],
		406: ['Unknown javascript error', 'Непонятная шибка в коде приложения.']
	};
	/*
  * Transport module
  * Used to send or wait for messages for unified socket-like module with 'on', 'off' and 'emit' methods.
  */
	// 5xx - Data completeness error
	// 6xx - Game logic error
	return Object.defineProperties({
		/*
   * Used to send socket message of specified type
   * @param {type} type of message
   * @param {data} message
   * @returns {Promise} resolve with value or reject with fail message
   */
		send: function send(transportType, type, data) {
			var _this = this;

			return Promise.race([$timeout(function () {
				return Promise.reject({ code: 402, description: 'Нет сети' });
			}, networkTimeout), new Promise(function (resolve, reject) {
				switch (transportType) {
					case _this.type.WEBSOCKET:
						socket.emit(type, data).on(type, resolve);
						break;
					case _this.type.POST:
					case _this.type.GET:
					case _this.type.PUT:
						http[transportType](type, data).then(function (_ref) {
							var status = _ref.status;
							var value = _ref.value;
							var description = _ref.description;
							var id = _ref.id;

							if (!status || status === 'failure') return Promise.reject({ description: description || 'Неизвестная ошибка', code: id });
							resolve(value);
						})['catch'](function (reason) {
							return reason && typeof reason === 'object' ? reject({ code: reason.code || -1, description: reason.description || 'Неизвестная ошибка' }) : reject({ code: 402, description: reason && reason.toString() || 'Нет сети' });
						});
						break;
				}
			})]).then(function (data) {
				if (transportType === _this.type.WEBSOCKET) socket.off(type);
				return Promise.resolve({ code: 100, data: data || {} });
			})['catch'](function (reason) {
				if (typeof reason === 'string') {
					if (/Uncaught.*Error:.*/.test(reason)) reason = { code: 405 };else reason = { code: 401 };
				} else if (reason.hasOwnProperty('code')) {
					if (reason.code === 5) reason.code = 500;
				} else reason = { code: 406 };
				return Promise.reject({
					code: reason.code,
					description: reason.description || errors[reason.code][1] || 'Неописанная ошибка.'
				});
			});
		},
		/*
   * Used to wait for socket response
   * @param {type} type of response
   * @returns {Promise} resolve with value or reject with fail message
   */
		wait: function wait(type) {
			return new Promise(function (resolve) {
				return socket.on(type, function response(data) {
					resolve(data);
					socket.off(type, response);
				});
			});
		},
		socketAuth: function socketAuth() {
			return socket.forceAuth();
		},
		stopSocket: function stopSocket() {
			return socket.forceStop();
		},

		type: {
			GET: 'get',
			POST: 'post',
			PUT: 'put',
			WEBSOCKET: 'ws'
		}
	}, {
		socketStatus: {
			get: function get() {
				return socket.isActive;
			},
			configurable: true,
			enumerable: true
		}
	});
});
'use strict';

angular.module('qlimate.utils').service('$u', function ($ionicBackdrop, $ionicPopup, $timeout) {
	var notCorrectNumber = function notCorrectNumber(obj) {
		return isNaN(Number(obj)) || Number(obj) === Number.POSITIVE_INFINITY || Number(obj) === Number.NEGATIVE_INFINITY;
	};
	var backdrop = false,
	    splash = false;
	var popups = [];
	var alert = {
		on: false,
		instance: null,
		message: '',
		title: 'Ошибка!'
	};
	return {
		correctNumber: function correctNumber() {
			for (var _len = arguments.length, rest = Array(_len), _key = 0; _key < _len; _key++) {
				rest[_key] = arguments[_key];
			}

			return rest.filter(notCorrectNumber).length === 0;
		},
		showSplash: function showSplash() {
			if (splash) return;
			splash = true;
			document.getElementById('qlimate-splash').style.visibility = 'visible';
		},
		hideSplash: function hideSplash() {
			if (!splash) return;
			splash = false;
			document.getElementById('qlimate-splash').style.visibility = 'hidden';
		},
		showBackdrop: function showBackdrop() {
			if (backdrop) return;
			backdrop = true;
			$ionicBackdrop.retain();
		},
		hideBackdrop: function hideBackdrop() {
			if (!backdrop) return;
			backdrop = false;
			$ionicBackdrop.release();
		},
		showPopup: function showPopup(_ref) {
			var buttons = arguments.length <= 1 || arguments[1] === undefined ? [{ text: 'Ок' }] : arguments[1];
			var title = _ref.title;
			var message = _ref.message;
			var destroyPolicy = arguments.length <= 2 || arguments[2] === undefined ? 'no' : arguments[2];

			// destroy mode: all, last, no
			// button: text, class, onClick
			if (destroyPolicy === 'all') {
				popups.forEach(function (popup) {
					return popup.close();
				});
				popups = [];
			} else if (destroyPolicy === 'last') popups.pop().close();
			$timeout(function () {
				popups.push($ionicPopup.show({
					title: title,
					cssClass: 'qlimate-popup',
					template: typeof message === 'string' ? message : JSON.stringify(message),
					buttons: buttons
				}));
			});
			return $timeout(function () {
				return new Promise(function (res, rej) {
					return popups[popups.length - 1].then(res);
				});
			}, 20);
		},
		showAlert: function showAlert(message) {
			return this.showPopup({ title: 'Ошибка', message: message }, undefined, 'all');
		}
	};
});
'use strict';

angular.module('qlimate.controllers.elements').directive('barrel', function () {
	return {
		restrict: 'E',
		templateUrl: 'templates/energy-bar.html',
		replace: true,
		scope: {
			items: '=',
			elementHeight: '=height', // like 15 for 15vw
			isTop: '=top',
			action: '&'
		},
		controller: function controller($scope, $element) {
			//let top = !($scope.isTop === 'false' || !scope.isTop);
			//let elementHeight = document.body.scrollWidth*$scope.elementHeight/100;
			//let min = $scope.isTop ?

			/*let min = 0, max, top, topRaw, topMargin = 10;
   top = topRaw = max = elementHeight*2;
   Object.defineProperty($scope.barrel, 'margin-top', { enumerable: true, get: () => `calc(${100 - topMargin}vh - ${Math.abs(topRaw)}px)`});
   $scope.doDrag = event => {
   	$scope.barrel['transition'] = '';
   	topRaw = Math.min(Math.max(max, top - event.gesture.deltaY), min);
   };
   $scope.doRelease = () => $scope.move(Math.min($scope.items.length - 1, Math.max(0, Math.round(topRaw/elementHeight) - 2)));
   $scope.move = id => {
   	top = topRaw = (id + 2)*elementHeight;
   	$scope.selectedIndex = id;
   	$scope.barrel['transition'] = 'all 0.2s ease-in';
   };
   		let elementHeight = document.body.scrollWidth*0.2; // HARDCODED: 20vw
   let min = elementHeight, max = 0, top = 0, topRaw = 0;
   Object.defineProperty($scope.barrel, 'margin-top', { enumerable: true, get: () => `calc(20vw - ${Math.abs(topRaw)}px)`});
   $scope.doDrag = event => {
   	$scope.barrel['transition'] = '';
   	topRaw = Math.min(Math.max(max, top - event.gesture.deltaY), min);
   };
   $scope.doRelease = () => $scope.move(Math.min($scope.items.length - 1, Math.max(0, Math.round(topRaw/elementHeight))));
   $scope.move = id => {
   	top = topRaw = id*elementHeight;
   	$scope.selectedIndex = id;
   	$scope.barrel['transition'] = 'all 0.2s ease-in';
   };*/
		}
	};
});
'use strict';

angular.module('qlimate.controllers.elements').directive('energyBar', function () {
	var buttonWidth = 80;
	return {
		restrict: 'E',
		templateUrl: 'templates/energy-bar.html',
		replace: true,
		controller: function controller($scope, $element, state) {
			$scope.max = 1;
			$scope.current = 0;
			var boltWidth = 6;
			var energyBoltWidth = $element[0].ownerDocument.body.children[0].clientHeight * 0.01 * boltWidth;
			var lightningsCount = Math.floor(($element[0].ownerDocument.body.clientWidth - energyBoltWidth * (2 /* menu button size */ + 4) /* energy available size */) / energyBoltWidth) - 1;
			$scope.total = new Array(lightningsCount);
			state.onUpdate(function (state) {
				return $scope.$apply(function () {
					$scope.max = state.max > 0 ? state.max : $scope.max;
					$scope.current = state.current > 0 ? state.current : $scope.current;
					$scope.pivot = $scope.current / $scope.max * $scope.total.length;
				});
			});
		}
	};
});
'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

angular.module('qlimate.controllers.elements').directive('scanButton', function ($cordovaBarcodeScanner, $ionicModal, $u, transport, toastr, location) {
	return {
		restrict: 'E',
		templateUrl: 'templates/scan-button.html',
		replace: true,
		controller: function controller($scope, $element, state) {
			$scope.max = 0;
			$scope.current = 0;
			state.onUpdate(function (state) {
				$scope.max = state.max;
				$scope.current = state.current;
			});

			var min = 800 - 185,
			    max = 800 - 185 - Math.round(($element[0].ownerDocument.body.scrollHeight - 185) / 2),
			    bottom = undefined,
			    bottomRaw = undefined;
			var reset = function reset() {
				bottom = bottomRaw = -min;
				$scope.energyPercent = 0.1;
			};
			reset();
			$scope.draggedStyle = Object.defineProperties({}, {
				bottom: {
					get: function get() {
						return bottomRaw + 'px';
					},
					configurable: true,
					enumerable: true
				}
			});
			$scope.doDrag = function (event) {
				bottomRaw = Math.max(Math.min(-max, bottom - event.gesture.deltaY), -min);
				$scope.energyPercent = Math.max((min + bottomRaw) / (min - max), 0.1);
			};
			$scope.doRelease = function (event) {
				return bottom = bottomRaw;
			};

			$ionicModal.fromTemplateUrl('templates/scan.html', {
				scope: $scope,
				backdropClickToClose: false
			}).then(function (modal) {
				return $scope.modal = modal;
			});

			$scope.scan = function ($event) {
				(window.cordova ? $cordovaBarcodeScanner.scan() : Promise.resolve({
					format: 'mock',
					cancelled: false,
					text: 'qlimate.com/15&1111'
					//text: 'asdkl;'
				})).then(function (_ref) {
					var format = _ref.format;
					var cancelled = _ref.cancelled;
					var text = _ref.text;

					if (cancelled) return Promise.reject({ code: 1, description: 'Сканирование отменено пользователем' });
					toastr.info('Barcode cancelled status ' + cancelled, 'Barcode format ' + format);
					var decoded = text.replace('qlimate.com/s?', '').split('&');
					return Promise.resolve(decoded.length === 2 ? decoded : ['', '']);
				}, function () {
					return Promise.reject({ code: 1, description: 'Сканирование отменено пользователем' });
				}).then(function (_ref2) {
					var _ref22 = _slicedToArray(_ref2, 2);

					var _ref22$0 = _ref22[0];
					var id = _ref22$0 === undefined ? '' : _ref22$0;
					var _ref22$1 = _ref22[1];
					var secret = _ref22$1 === undefined ? '' : _ref22$1;

					if (!id || id.length > 12 || !secret || secret.length !== 4) return Promise.reject({ code: 500, description: 'Сканированный QR-код некорректен.' });
					if ($scope.energyPercent > 1) return Promise.reject({ code: 500, description: 'Некорректное количество энергии.' });
					return transport.send(transport.type.POST, 'scan', { id: id, secret: secret, energy: Math.max(Math.round($scope.energyPercent * $scope.current), 1), lat: location.lat, lng: location.lng });
				})['catch'](function () {
					var _ref3 = arguments.length <= 0 || arguments[0] === undefined ? {} : arguments[0];

					var code = _ref3.code;
					var description = _ref3.description;

					toastr.warning('Barcode was not scanned: ' + description);
					if (code === 4325) description = 'Неправильный QR-код! Попробуй еще раз.';
					return Promise.reject({
						code: code || 400,
						description: description || 'Кажется, отсутствует сеть'
					});
				}).then(function (_ref4) {
					var data = _ref4.data;

					toastr.info('QR data response: ' + JSON.stringify(data));
					//data = {"delta":{"player":{"energy":-1,"capacity":0.01},"terminal":{"temperature":-0.5}}};
					$scope.scanResults = [];
					if (data.delta) {
						if (data.delta.player) {
							if (data.delta.player.energy) $scope.scanResults.push({
								title: 'Энергия игрока изменена',
								description: 'Ты потратил ' + (-data.delta.player.energy || 0) + ' энергии!'
							});

							if (data.delta.player.capacity) $scope.scanResults.push({
								title: 'Максимальная энергия игрока изменена',
								description: 'Теперь у тебя может быть больше на ' + (data.delta.player.capacity || 0) + ' энергии!'
							});
						}
						if (data.delta.terminal) {
							if (data.delta.terminal.temperature) $scope.scanResults.push({
								title: 'Температура терминала изменена',
								description: 'Температура терминала ' + (data.delta.terminal.temperature < 0 ? 'уменьшилась' : 'увеличилась') + ' на ' + (data.delta.terminal.temperature || 0) + '!'
							});
						}
					}
					$scope.modal.show($event);
					reset();
				})['catch'](function (_ref5) {
					var code = _ref5.code;
					var description = _ref5.description;

					code === 1 ? null : $u.showAlert(description);
				});
			};
		}
	};
});
'use strict';

angular.module('qlimate.controllers.pages').controller('AuthCtrl', function ($scope, storage, state, $state, transport, $u, socket, toastr) {
	$scope.$on('$ionicView.afterEnter', function () {
		toastr.info('PAGE ||| Login');
		if (storage.phone) $scope.phone = storage.phone.toString();
		$scope.showPin = false;
	});
	$scope.storage = storage;
	$scope.doRequestPin = function (phone) {
		$u.showBackdrop();
		if ($scope.nickname) storage.nickname = $scope.nickname;
		storage.phone = phone;
		transport.send(transport.type.POST, 'register', { phone: phone }).then(function (_ref) {
			var _ref$data = _ref.data;
			_ref$data = _ref$data === undefined ? {} : _ref$data;
			var code = _ref$data.code;

			$scope.$apply(function () {
				$scope.showPin = true;
				$scope.pin = Number(code) || '';
			});
		})['catch'](function (_ref2) {
			var description = _ref2.description;
			var code = _ref2.code;

			$u.showAlert(description);
		}).then(function () {
			return $u.hideBackdrop();
		});
	};
	$scope.doConfirm = function (phone, code) {
		$u.showBackdrop();
		transport.send(transport.type.POST, 'confirm', {
			phone: phone,
			code: code,
			team: storage.intendedTeam === 1 ? 'orange' : storage.intendedTeam === -1 ? 'blue' : 'neutral'
		}).then(function (_ref3) {
			var _ref3$data = _ref3.data;
			_ref3$data = _ref3$data === undefined ? {} : _ref3$data;
			var token = _ref3$data.token;

			storage.nickname = storage.nickname || $scope.nickname || $scope.generate;
			storage.token = token;
			$state.go('preload');
		})['catch'](function (_ref4) {
			var description = _ref4.description;
			var code = _ref4.code;

			$scope.$apply(function () {
				$scope.showPin = false;
				$scope.pin = '';
			});
			if (code === 4312) {
				$u.showAlert('Вы не выбрали команду!');
				storage.team = null;
				return $state.go('preload');
			}
			$u.showAlert(description);
			// TODO: Ошибка "ник занят"
		}).then(function () {
			return $u.hideBackdrop();
		});
	};
	$scope.changeTeam = function (color) {
		return storage.intendedTeam = color;
	};

	$scope.generate = (function () {
		var adj = ['Зимний', 'Красный', 'Черный', 'Огненный'];
		var noun = ['волк', 'лев', 'олень', 'кракен', 'дракон', 'орел', 'змей'];
		return adj[Math.floor(Math.random() * adj.length)] + ' ' + noun[Math.floor(Math.random() * noun.length)];
	})();
}).directive('formatPhone', function () {
	return {
		require: '?ngModel',
		link: function link(scope, elem, attrs, ctrl) {
			if (!ctrl) return;
			ctrl.$formatters.unshift(function (modelValue) {
				return (modelValue || '').replace(/^(7)(\d{3})(.+)/, '+$1($2)$3') // Add braces around city code
				.replace(/^(\+7.{5})(\d{3})(.+)/g, '$1$2-$3') // Add first dash
				.replace(/^(\+7.{9})(\d{2})(.+)/g, '$1$2-$3');
			});
			ctrl.$parsers.unshift(function (viewValue) {
				var phone = viewValue.replace(/([^\d+]|\+)/g, '') // Remove garbage symbols w/ not first plus signes
				.replace(/^([0-69])$/, '7$1') // Add 7 at the beginning
				.replace(/^8(\d)/, '7$1') // Modify '8917..' to '7917..'
				.substring(0, 11);
				elem.val(ctrl.$formatters[0](phone));
				return phone;
			});

			ctrl.$validators.phone = function (model) {
				return !!(model && model.length === 11);
			};
		}
	};
});
'use strict';

angular.module('qlimate.controllers.pages').controller('ChooseCtrl', function ($scope, state, storage, $state, $u, toastr) {
	$scope.$on('$ionicView.afterEnter', function () {
		toastr.info('PAGE ||| Choose');
		if (storage.team || storage.intendedTeam) $scope.setTeam(storage.team || storage.intendedTeam);
	});
	$scope.setTeam = function (color) {
		storage.intendedTeam = color;
		$state.go('preload');
	};
});
'use strict';

angular.module('qlimate.controllers.pages').controller('FeedbackCtrl', function ($scope, transport, $u, $state) {
	$scope.send = function () {
		if (!$scope.feedback) return $u.showAlert('Письмо пустое!');
		console.log($scope);
		$u.showBackdrop();
		transport.send(transport.type.PUT, 'feedback', {
			message: $scope.feedback || ''
		}).then(function () {
			$u.hideBackdrop();
			$scope.feedback = '';
			$u.showPopup({ title: 'Супер', message: 'Письмо ушло! Жди ответа.' }).then(function () {
				return $state.go('menu.map');
			});
		}, function () {
			$u.hideBackdrop();
			$u.showAlert('Мы не смогли отправить письмо. Попробуй еще раз.');
		});
	};
});
'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

angular.module('qlimate.controllers.pages').controller('MapCtrl', function ($scope, $ionicPopover, terminals, state, $cordovaBarcodeScanner, transport, $u, $timeout, toastr, location) {
	$scope.$on('$ionicView.afterEnter', function () {
		toastr.info('PAGE ||| Map');
		state.forceUpdate();
	});
	document.getElementById('map').onmousedown = function (e) {
		return e.preventDefault() & false;
	};

	mapboxgl.accessToken = 'pk.eyJ1IjoibHVpeG8iLCJhIjoiZjlhMTVlOGY2N2MyNjUxM2FhOGYyZjgyZGYwODdjNzIifQ.5TM2WTaF4_Y08t6K68gnNw';
	$scope.map = new mapboxgl.Map({
		container: 'map',
		style: state.current.team === -1 ? 'mapbox://styles/luixo/cih7etdg0002bbkko2mhpmlh0' : 'mapbox://styles/luixo/cii7j48nk00grbpm0kw6pqfg4',
		center: [37.5972, 55.6372],
		zoom: 9
	});
	$scope.map.addControl(new mapboxgl.Navigation({ position: 'bottom-left' }));
	$scope.map.dragRotate && $scope.map.dragRotate.disable();
	$scope.map.touchZoomRotate && $scope.map.touchZoomRotate.disableRotation();

	state.onUpdate(function (_ref) {
		var team = _ref.team;

		if (team !== $scope.team) $scope.map.setStyle(state.current.team === -1 ? 'mapbox://styles/luixo/cih7etdg0002bbkko2mhpmlh0' : 'mapbox://styles/luixo/cii7j48nk00grbpm0kw6pqfg4');
		$scope.team = team;
	});

	var updateTerms = function updateTerms(position) {
		Promise.all([terminals.list(position), new Promise(function (resolve) {
			return $scope.map.style.loaded() ? resolve() : $scope.map.on('style.load', resolve);
		})]).then(function (_ref2) {
			var _ref22 = _slicedToArray(_ref2, 1);

			var _ref22$0 = _ref22[0];
			var plrs = _ref22$0.plrs;
			var terms = _ref22$0.terms;

			var data = {
				type: 'FeatureCollection',
				features: Object.values(terms).map(function (_ref3) {
					var id = _ref3.id;
					var description = _ref3.description;
					var position = _ref3.position;
					var _ref3$temperature = _ref3.temperature;
					var temperature = _ref3$temperature === undefined ? -0.5 + Math.random() : _ref3$temperature;
					return {
						type: 'Feature',
						geometry: {
							type: 'Point',
							coordinates: [position.lng, position.lat]
						},
						properties: {
							title: 'Terminal',
							'marker-symbol': 'terminal-' + (temperature === 0 ? 'neutral' : temperature > 0 ? 'hot' : 'cold'),
							id: id,
							description: description,
							temperature: temperature
						}
					};
				})
			};
			$scope.map.getSource('markers') ? $scope.map.getSource('markers').setData(data) : $scope.map.addSource('markers', {
				type: 'geojson',
				data: data,
				cluster: true,
				clusterRadius: 40
			});

			if (!$scope.map.getLayer('markers')) $scope.map.addLayer({
				id: 'markers',
				interactive: true,
				type: 'symbol',
				source: 'markers',
				layout: {
					'icon-image': '{marker-symbol}',
					'icon-allow-overlap': true
				}
			});

			if (!$scope.map.getLayer('markers-cluster')) $scope.map.addLayer({
				id: 'markers-cluster',
				interactive: true,
				type: 'symbol',
				source: 'markers',
				layout: { 'icon-image': 'terminals' },
				filter: ['>', 'point_count', 1]
			});
		}, function (error) {});
	};
	location.update().then(updateTerms);
	$scope.map.on('moveend', function (_ref4) {
		var target = _ref4.target;
		return updateTerms(target.getCenter());
	});

	var zoomHelper = null,
	    mapPopup = null;
	$scope.map.on('zoomstart', function (e) {
		return zoomHelper = e.target.getZoom();
	});
	$scope.map.on('zoomend', function (e) {
		return mapPopup && zoomHelper < e.target.getZoom() ? mapPopup.remove() : null;
	});

	var onClick = function onClick(e) {
		$scope.map.featuresAt(e.point, {
			radius: 25,
			layer: 'markers'
		}, function (err, features) {
			features = features.reduce(function (mem, feature) {
				return feature.properties.cluster ? mem.concat(feature.properties.points) : mem.concat([feature]);
			}, []);
			if (features.length) {
				mapPopup = new mapboxgl.Popup().setLngLat(e.lngLat).setHTML('\n\t\t\t\t\t\t<div class="head">Терминалы</div>\n\t\t\t\t\t\t<dl>\n\t\t\t\t\t\t\t' + features.reduce(function (memo, _ref5, i) {
					var term = _ref5.properties;
					return memo + (i === 5 ? '<div class="ion-search"></div>' : i < 5 ? '\n\t\t\t\t\t\t\t<dt class="' + (term.temperature === 0 ? 'neutral' : term.temperature > 0 ? 'hot' : 'cold') + '">' + term.description.replace(/^Россия,? ?/, '').replace(/^Москва,? ?/, '') + '</dt>\n\t\t\t\t\t\t\t<dd>' + Math.round(term.temperature) + '°</dd>\n\t\t\t\t\t\t' : '');
				}, '') + '</dl>\n\t\t\t\t\t').addTo($scope.map);
				(document.querySelectorAll('.mapboxgl-popup .ion-search')[0] || {}).onclick = function () {
					$scope.map.flyTo({ center: [e.lngLat.lng, e.lngLat.lat], zoom: $scope.map.getZoom() + 1 });
				};
			}
		});
	};
	$scope.map.on('click', function (e) {
		return onClick(e);
	});

	$u.showBackdrop();
	// HACK: убрали копирайт. Юридически это неправильно.
	$scope.map._controlCorners['bottom-right'].innerHTML = '';
	$scope.findMe = function () {
		return location.update().then(function (_ref6) {
			var lng = _ref6.lng;
			var lat = _ref6.lat;
			var accuracy = _ref6.accuracy;
			return $scope.map.flyTo({ center: [lng, lat], zoom: accuracy === 0 ? 10 : 16 - accuracy / 500 });
		});
	};
	$scope.findMe();
	Promise.loop(function (resolve, reject) {
		return $timeout(function () {
			return transport.socketStatus ? reject() : resolve(transport.socketAuth());
		}, 300);
	})['catch'](function () {
		return $u.hideBackdrop();
	});
});
'use strict';

angular.module('qlimate.controllers.pages').controller('PreloadCtrl', function ($scope, storage, $state, state, transport, $u, socket, toastr) {
	var REQUIRED = [{
		ready: function ready() {
			return storage.intendedTeam !== undefined;
		},
		resolve: function resolve() {
			return $state.go('choose-side');
		}
	}, {
		ready: function ready() {
			return state.authed;
		},
		resolve: function resolve() {
			if (storage.token && storage.phone) transport.send(transport.type.POST, 'login', {
				phone: storage.phone,
				token: storage.token
			}).then(function () {
				state.authed = true;
				state.forceUpdate().then(function () {
					return state.current.name ? null : transport.send(transport.type.PUT, 'profile', { name: storage.nickname });
				}).then(function () {
					return storage.addPhoneNamePair(storage.phone, state.current.name);
				})['catch'](function (e) {
					console.log('PUT on profile failed: ' + JSON.stringify(e));
				});
			}, function () {
				return storage.token = null;
			}).then(function () {
				return $scope.$emit('$ionicView.afterEnter');
			});else $state.go('auth');
		}
	}];
	$scope.$on('$ionicView.afterEnter', function () {
		toastr.info('PAGE ||| Preload');
		var left = REQUIRED.filter(function (condition) {
			return !condition.ready();
		});
		if (left.length) return left[0].resolve();
		transport.socketAuth();
		$state.go('menu.map');
	});
});
'use strict';

angular.module('qlimate.controllers.pages').controller('ProfileCtrl', function ($scope, transport, state, icons, $u) {
	state.onUpdate(function (_ref) {
		var name = _ref.name;
		var phone = _ref.phone;
		var team = _ref.team;

		$scope.team = team;
		$scope.name = name;
		$scope.chars = { 'Телефон': phone };
	});

	$scope.iconMap = icons.items;
	$scope.items = [];
	$scope.selectedIndex = 0;
	$scope.barrel = {};

	$scope.$on('$ionicView.afterEnter', function () {
		$scope.placeholder = 'Загружаем предметы..';
		transport.send(transport.type.GET, 'inventory').then(function (_ref2) {
			var data = _ref2.data;

			$scope.items = data;
			min = elementHeight * ($scope.items.length + 1);
			top = topRaw = min;
		}, function (_ref3) {
			var code = _ref3.code;
			var description = _ref3.description;

			if (code === 402) $scope.placeholder = 'Нет сети';else $u.showAlert(description);
		});
	});

	var elementHeight = document.body.scrollWidth * 0.15; // HARDCODED: 15vw
	var min = 0,
	    max = undefined,
	    top = undefined,
	    topRaw = undefined,
	    topMargin = 10;
	top = topRaw = max = elementHeight * 2;
	Object.defineProperty($scope.barrel, 'margin-top', { enumerable: true, get: function get() {
			return 'calc(' + (100 - topMargin) + 'vh - ' + Math.abs(topRaw) + 'px)';
		} });
	$scope.doDrag = function (event) {
		$scope.barrel['transition'] = '';
		topRaw = Math.min(Math.max(max, top - event.gesture.deltaY), min);
	};
	$scope.doRelease = function () {
		return $scope.move(Math.min($scope.items.length - 1, Math.max(0, Math.round(topRaw / elementHeight) - 2)));
	};
	$scope.move = function (id) {
		top = topRaw = (id + 2) * elementHeight;
		$scope.selectedIndex = id;
		$scope.barrel['transition'] = 'all 0.2s ease-in';
	};

	$scope.use = function () {
		return $u.showPopup({ title: 'Внимание', message: 'К сожалению, этот функционал пока не реализован.' });
		$u.showBackdrop();
		transport.send(transport.type.PUT, 'inventory', {
			index: $scope.items[$scope.selectedIndex].index
		}).then(function () {
			$u.showPopup({ title: 'Успех', message: 'Предмет использован!' });
		}, function () {
			$u.showAlert('К сожалению, не получилось использовать предмет.');
		}).then(function () {
			return $u.hideBackdrop();
		});
	};
});
'use strict';

angular.module('qlimate.controllers.pages').controller('ShopCtrl', function ($scope, $timeout, transport, $u, icons, state) {
	$scope.iconMap = icons.items;
	$scope.items = [];
	$scope.selectedIndex = 0;
	$scope.barrel = {};

	$scope.$on('$ionicView.afterEnter', function () {
		$scope.placeholder = 'Загружаем магазин..';
		transport.send(transport.type.GET, 'store').then(function (_ref) {
			var data = _ref.data;

			$scope.$apply(function () {
				return $scope.items = data;
			});
			min = elementHeight * ($scope.items.length - 1);
		}, function (_ref2) {
			var code = _ref2.code;
			var description = _ref2.description;

			if (code === 402) $scope.placeholder = 'Нет сети';else $u.showAlert(description);
		});
	});

	var elementHeight = document.body.scrollWidth * 0.2; // HARDCODED: 20vw
	var min = elementHeight,
	    max = 0,
	    top = 0,
	    topRaw = 0;
	Object.defineProperty($scope.barrel, 'margin-top', { enumerable: true, get: function get() {
			return 'calc(20vw - ' + Math.abs(topRaw) + 'px)';
		} });
	$scope.doDrag = function (event) {
		$scope.barrel['transition'] = '';
		topRaw = Math.min(Math.max(max, top - event.gesture.deltaY), min);
	};
	$scope.doRelease = function () {
		return $scope.move(Math.min($scope.items.length - 1, Math.max(0, Math.round(topRaw / elementHeight))));
	};
	$scope.move = function (id) {
		top = topRaw = id * elementHeight;
		$scope.selectedIndex = id;
		$scope.barrel['transition'] = 'all 0.2s ease-in';
	};

	$scope.buy = function () {
		$u.showBackdrop();
		transport.send(transport.type.POST, 'inventory', {
			index: $scope.items[$scope.selectedIndex].index
		}).then(function () {
			$u.showPopup({ title: 'Успех', message: $scope.items[$scope.selectedIndex].name + ' куплен! Ищи его в инвентаре.' });
			$scope.items[$scope.selectedIndex].count--;
			if ($scope.items[$scope.selectedIndex].count === 0) $scope.items.splice($scope.selectedIndex, 1);
		}, function (_ref3) {
			var code = _ref3.code;
			var description = _ref3.description;

			$u.showAlert(code === 4316 ? 'К сожалению, такие предметы кончились' : 'К сожалению, не получилось купить предмет.');
		}).then(function () {
			return $u.hideBackdrop();
		});
	};
});
'use strict';

angular.module('qlimate.controllers.pages').controller('TournamentCtrl', function ($scope, transport, $u, $state) {
	$scope.$on('$ionicView.afterEnter', function () {
		transport.send(transport.type.GET, 'tournaments').then(function (_ref) {
			var data = _ref.data;

			$scope.tournaments = data.items;
		});
	});

	$scope.$on('$ionicView.beforeEnter', function () {
		return $u.showPopup({ title: 'Внимание!', message: 'Раздел в разработке. Приносим свои извинения.' }).then(function () {
			return $state.go('menu.map');
		});
	});
});
'use strict';

angular.module('qlimate.mock').service('socket', function ($rootScope, $timeout, mockData, toastr) {
	var events = {};
	return {
		on: function on(type, callback) {
			return (events[type] || (events[type] = new Set())).add(callback);
		},
		off: function off(type, callback) {
			return events[type] && events[type]['delete'](callback);
		},
		emit: function emit(type, data) {
			toastr.info('MOCK >>>', type + ': ' + JSON.stringify(data));
			if (mockData[type]) mockData[type](events, data);
			return this;
		},
		forceAuth: function forceAuth() {
			this.emit('auth');
		},
		isActive: true
	};
}).service('http', function ($rootScope, $timeout, mockData, toastr) {
	var response = function response(type, data) {
		toastr.info('MOCK >>>', type + ': ' + JSON.stringify(data));
		return new Promise(function (resolve, reject) {
			toastr.info('MOCK <<<', type + ': ' + JSON.stringify(mockData[type] ? mockData[type](data) : 'No such mock'));
			$timeout(function () {
				return mockData[type] ? resolve({
					type: type,
					status: 'success',
					value: mockData[type](data),
					description: 'Successfully mocked!'
				}) : reject({
					type: type,
					status: 'failure',
					description: 'No mock data'
				});
			}, 300);
		});
	};
	return {
		post: response,
		put: response,
		get: response
	};
});
'use strict';

angular.module('qlimate.mock').service('mockData', function ($timeout, toastr) {
	return {
		// Socket mocks
		'auth': function auth(events, data) {
			toastr.info('MOCK <<<', 'auth');
			(events['auth'] || []).forEach(function (cb) {
				return cb();
			});
			setInterval(function () {
				toastr.info('MOCK <<<', 'update');
				(events['update'] || []).forEach(function (cb) {
					cb({
						state: {
							energy: 100 + Math.random() * 20,
							capacity: 200 + Math.random() * 10,
							bonus: 20,
							penalty: 10
						}
					});
				});
			}, 5000);
			setInterval(function () {
				toastr.info('MOCK <<<', 'notification');
				(events['notification'] || []).forEach(function (cb) {
					cb({
						from: 'system',
						title: 'Текущее время: ' + Date.now(),
						body: 'Спасибо что были с нами!'
					});
				});
			}, 20000);
		},
		'map': function map() {
			var terminals = [];
			var d = 0.16;
			var c = {
				lat: 55.7511,
				lng: 37.6204
			};
			for (var i = 0; i < 100; i++) {
				var r = Math.random() * d;
				terminals.push({
					position: {
						x: c.lat + (Math.random() < 0.5 ? -1 : 1) * r,
						y: c.lng + (Math.random() < 0.5 ? -1 : 1) * Math.random() * Math.sqrt(d * d - r * r)
					},
					temperature: Math.round(Math.random() * 800 - 400),
					id: Math.round(Math.random() * 10000)
				});
			}
			return { terminals: terminals };
		},
		'scan': function scan() {
			return {
				results: [{
					title: 'Энергия',
					description: 'Энергия терминала 123 увеличена на 321',
					icon: 2,
					isNew: true
				}, {
					title: 'Достижение',
					description: 'Получено достижение "Пересмешник"',
					icon: 1,
					isNew: false
				}]
			};
		},
		'shop': function shop() {
			return [{
				name: 'Суперштука',
				description: 'Суперштукает предметы.',
				cost: 200
			}, {
				name: 'Суперштук 2',
				description: 'Суперштукает предметы.',
				cost: 100
			}, {
				name: 'Суперштука 3',
				description: 'Суперштукает предметы.',
				cost: 300
			}, {
				name: 'Суперштука 4',
				description: 'Суперштукает предметы.',
				cost: 400
			}];
		},
		// HTTP mocks
		'login': function login(_ref) {
			var phone = _ref.phone;
			var token = _ref.token;
			return {
				profile: 'nwww',
				info: 'zzz'
			};
		},
		'register': function register(_ref2) {
			var phone = _ref2.phone;
			return { code: '12345' };
		},
		'confirm': function confirm(_ref3) {
			var pin = _ref3.pin;
			return { token: 'hello' };
		},
		'profile': function profile() {
			return {
				name: 'bot13',
				phone: '7917530974378123123',
				team: 'orange'
			};
		},
		'info': function info() {
			return {
				game: {
					description: 'Описание игры',
					legend: 'Легенда игры'
				},
				teams: {
					orange: {
						population: 100,
						description: 'Команда огня',
						index: 1.4
					},
					blue: {
						population: 200,
						description: 'Команда льда',
						iindex: 1.2
					}
				}
			};
		}
	};
});
//# sourceMappingURL=all.js.map
