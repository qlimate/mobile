var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');
var plumber = require('gulp-plumber');
var shell = require('gulp-shell');
var replace = require('gulp-replace');
var version = require('./package.json').version;

var paths = {
	sass: ['./scss/*.scss'],
	src: ['./jssrc/**/*.js'],
	html: ['./jssrc/**/*.html']
};

gulp.task('babel', () => gulp.src(paths.src)
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(babel())
		.pipe(concat('all.js'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest('./www')));

gulp.task('html', () => gulp.src(paths.html).pipe(gulp.dest('./www')));

gulp.task('sass', () => sass(['./scss/ionic.app.scss', './scss/qlimate.scss'], {compass: true, })
		.pipe(gulp.dest('./www/css/')));

gulp.task('watch', () => {
	gulp.watch(paths.sass, ['sass']);
	gulp.watch(paths.src, ['babel']);
	gulp.watch(paths.html, ['html']);
});

gulp.task('replace-version', () => gulp.src(['./config.xml'])
	.pipe(replace(/(<widget id="[a-z\.]*" version=")[0-9\.]*(" xmlns=".*".*>)/, `$1${version}$2`))
	.pipe(gulp.dest('.')));
gulp.task('replace-version-ingame', () => gulp.src(['./www/all.js'])
	.pipe(replace('%version%', version))
	.pipe(gulp.dest('./www/')));

gulp.task('build-android', shell.task([
	'gulp replace-version replace-version-ingame',
	'gulp sass babel html',
	`del Qlimate-*.apk`,
	'ionic build android --release',
	'copy platforms\\android\\build\\outputs\\apk\\android-x86-release-unsigned.apk',
	'copy platforms\\android\\build\\outputs\\apk\\android-armv7-release-unsigned.apk',
	'jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore -storepass qiwiqlimate android-x86-release-unsigned.apk qlimate',
	'jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore -storepass qiwiqlimate android-armv7-release-unsigned.apk qlimate',
	`del Qlimate-x86-${version}.apk`,
	`del Qlimate-armv7-${version}.apk`,
	`zipalign -v 4 android-x86-release-unsigned.apk Qlimate-x86-${version}.apk`,
	`zipalign -v 4 android-armv7-release-unsigned.apk Qlimate-armv7-${version}.apk`,
	`del android-x86-release-unsigned.apk`,
	`del android-armv7-release-unsigned.apk`
], {
	verbose: true,
	errorMessage: `Command <%= command %> failed with exit codes <%= error.code %>`,
}));